console.log("Hello from progress.js!");
setInterval(() => {
	console.log("from callback")
	const Http = new XMLHttpRequest();
	const url='hourlies/progress.json';
	Http.open("GET", url);
	Http.send();
	Http.onreadystatechange=(e)=>{
		// console.log("e: " + JSON.stringify(e)); // isTruted: true
		console.log("responseText: " + Http.responseText);
		let json = "";
		try {
			json = JSON.parse(Http.responseText);
		} catch (err) {
			console.log("error: " + JSON.stringify(err));
		}
		const element = document.getElementById("progress");
		element.style.width = "" + json.progress + "%";
	}
}, 500);