class EmissionsController < ApplicationController

  def index

  end

  def show
    if valid? then
      normalize_time_range_needed
      @columns_human = [
        # ["Monitoring","CEMS10_MonitoringCode",nil],
        ["Process","CEMS10_ProcessCode",nil],
        ["Outlet NOx","CEMS10_Stack_NOx_EmRate_HDM_Hourly","lb/hr"],
        ["Outlet NOx Excess w/o Burner","outlet_nox_excess_without","lb/hr > #{LIMIT_WITHOUT_BURNER_HOURLY}"],
        ["Outlet NOx Excess with Burner","outlet_nox_excess_with","lb/hr > #{LIMIT_WITH_BURNER_HOURLY}"],
        ["Outlet NOx Energy","CEMS10_Stack_NOxMWh_Ratio_HDM_Hourly","lb/MWhr"],
      ]
      get_from_prism
    else
      redirect_to action: "index"
    end
  end

  private
  
end
