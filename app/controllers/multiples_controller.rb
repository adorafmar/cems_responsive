class MultiplesController < ApplicationController
  
  def index

  end

  def show
    normalize_time_range_needed
    calibration_events = get_calibration_events

    @headers = [
      ["analyzer",nil],
      ["type",nil],
      ["when",nil],
      ["expected","ppm"],
      ["actual","ppm"],
      ["error","ppm"],
      ["limit","ppm"],
      ["5Day","count"],
    ]
    @tags = [
      ["","CEMS10_Stack_NO_Setup_Expected_Zero"],
      ["","CEMS10_Stack_NO_Setup_Expected_Span"],
      ["","CEMS10_Stack_NO_Setup_Zero_Actual"],
      ["","CEMS10_Stack_NO_Setup_Span_Actual"],
      ["","CEMS10_Stack_NO_Setup_Zero_Error"],
      ["","CEMS10_Stack_NO_Setup_Span_Error"],
      ["","CEMS10_Stack_NO_Setup_Alarm_Limit"],
      ["","CEMS10_Stack_Analyzer_NO_5Day_AL"],

      ["","CEMS10_Stack_CO_Setup_Expected_Zero"], 
      ["","CEMS10_Stack_CO_Setup_Expected_Span"],
      ["","CEMS10_Stack_CO_Setup_Zero_Actual"],
      ["","CEMS10_Stack_CO_Setup_Span_Actual"],
      ["","CEMS10_Stack_CO_Setup_Zero_Error"],
      ["","CEMS10_Stack_CO_Setup_Span_Error"],
      ["","CEMS10_Stack_CO_Setup_Alarm_Limit"],
      ["","CEMS10_Stack_Analyzer_CO_5Day_AL"],

      ["","CEMS10_Stack_NO2_Setup_Expected_Zero"],
      ["","CEMS10_Stack_NO2_Setup_Expected_Span"],
      ["","CEMS10_Stack_NO2_Setup_Zero_Actual"],
      ["","CEMS10_Stack_NO2_Setup_Span_Actual"],
      ["","CEMS10_Stack_NO2_Setup_Zero_Error"],
      ["","CEMS10_Stack_NO2_Setup_Span_Error"],
      ["","CEMS10_Stack_NO2_Setup_Alarm_Limit"],
      ["","CEMS10_Stack_Analyzer_NO2_5Day_AL"],

      ["","CEMS10_NH3_Ana_Setup_Expected_Zero"],
      ["","CEMS10_NH3_Ana_Setup_Expected_Span"],
      ["","CEMS10_NH3_Ana_Setup_Zero_Actual"],
      ["","CEMS10_NH3_Ana_Setup_Span_Actual"],
      ["","CEMS10_NH3_Ana_Setup_Zero_Error"],
      ["","CEMS10_NH3_Ana_Setup_Span_Error"],
      ["","CEMS10_NH3_Ana_Setup_Alarm_Limit"],

      ["","CEMS10_Inlet_NO2_Setup_Expected_Zero"],
      ["","CEMS10_Inlet_NO2_Setup_Expected_Span"],
      ["","CEMS10_Inlet_NO2_Setup_Zero_Actual"],
      ["","CEMS10_Inlet_NO2_Setup_Span_Actual"],
      ["","CEMS10_Inlet_NO2_Setup_Zero_Error"],
      ["","CEMS10_Inlet_NO2_Setup_Span_Error"],
      ["","CEMS10_Inlet_NO2_Setup_Alarm_Limit"],

      ["","CEMS10_Inlet_NO_Setup_Expected_Zero"],
      ["","CEMS10_Inlet_NO_Setup_Expected_Span"],
      ["","CEMS10_Inlet_NO_Setup_Zero_Actual"],
      ["","CEMS10_Inlet_NO_Setup_Span_Actual"],
      ["","CEMS10_Inlet_NO_Setup_Zero_Error"],
      ["","CEMS10_Inlet_NO_Setup_Span_Error"],
      ["","CEMS10_Inlet_NO_Setup_Alarm_Limit"],
    ]
    analyzers = []
    fragments = ["CEMS10","Setup","Zero","Span","5Day","AL","Expected","Actual","Error","Alarm","Limit","Analyzer","Ana"]
    @tags.each do |tag|
      analyzer = tag[1]
      fragments.each do |fragment|
        analyzer = analyzer.remove(/#{fragment}/)
      end
      analyzers << analyzer
    end
    analyzers.uniq!
    analyzers.each_index do |ndx|
      analyzers[ndx] = analyzers[ndx].split("_").uniq
      analyzers[ndx].delete("")
    end
    analyzers.delete([])
    analyzers.each_index do |ndx|
      analyzers[ndx] = analyzers[ndx].join(" ")
    end
    analyzers.uniq!
    normalize_time_range_needed
    @columns = []
    @tags.each do |tag|
      @columns << tag[1] if not tag[1].empty?
    end
    last_error = nil
    @responses = Array.new
    columns_to_request = @columns[0 .. @columns.length]
    columns_to_request.delete_if {|column| column.empty?} 

    puts "looping"
    while columns_to_request.length != 0
      puts "  columns_to_request.length: " + columns_to_request.length.to_s
      @tags_to_request = columns_to_request.shift(10)
      eliminate_bad_tags
      @tags_to_request = @tags_to_request.join(",")
      puts "  tags_to_request: " + @tags_to_request
      get_data
    end
    data_received = Hash.new
    response_i = 0
    @responses.each do |response|
      column_names = response.keys
      column_names.each do |column_name|
        if data_received[column_name].nil?
          data_received[column_name] = response[column_name]
        else
          data_received[column_name].concat(response[column_name])
        end
        puts response_i.to_s + " " + column_name + " c: " + data_received[column_name].length.to_s + " r: " + response[column_name].length.to_s
      end
      response_i = response_i + 1
    end
    nDatum = data_received["CEMS10_Stack_NO_Setup_Expected_Zero"].length
    locations = ["Inlet","Stack"]
    @records = Array.new
    analyzers.each do |analyzer|
      ["Zero","Span"].each do |z_s|
        puts analyzer + " " + z_s
        tag_expected = data_received.keys.grep(/#{analyzer.split(" ")[0]}/).grep(/#{analyzer.split(" ")[1]}_/).grep(/#{z_s}/).grep(/Expected/)[0]
        tag_actual = data_received.keys.grep(/#{analyzer.split(" ")[0]}/).grep(/#{analyzer.split(" ")[1]}_/).grep(/#{z_s}/).grep(/Actual/)[0]
        tag_error = data_received.keys.grep(/#{analyzer.split(" ")[0]}/).grep(/#{analyzer.split(" ")[1]}_/).grep(/#{z_s}/).grep(/Error/)[0]
        tag_limit = data_received.keys.grep(/#{analyzer.split(" ")[0]}/).grep(/#{analyzer.split(" ")[1]}_/).grep(/Limit/)[0]
        tag_5Day = data_received.keys.grep(/#{analyzer.split(" ")[0]}/).grep(/#{analyzer.split(" ")[1]}_/).grep(/5Day/)[0]
        puts tag_expected, tag_actual, tag_error, tag_limit, tag_5Day
        time_zero = Time.at(data_received[tag_expected][0]["T"]/1000)
        time_span = Time.at(data_received[tag_expected][nDatum-2]["T"]/1000)
        when_ = z_s == "Zero" ? time_zero : time_span
        value_expected = data_received[tag_expected][0]["V"].round(2)
        value_actual = data_received[tag_actual][nDatum-1]["V"].round(2)
        value_error = data_received[tag_error][nDatum-1]["V"].round(2)
        value_limit = data_received[tag_limit][nDatum-1]["V"].round(2)
        value_5Day = data_received[tag_5Day][nDatum-1]["V"] if not tag_5Day.nil?
        when_ = when_.strftime("%m/%d/%Y %H:%M")
        record = Array[analyzer,z_s,when_,value_expected,value_actual,value_error,value_limit,value_5Day,""]
        @records << record
      end
    end
  end

  private

    def get_calibration_events
      puts "get_calibration_events"
      tags_to_request = "CEMS10_SCR_Inlet_Cal_Seq_Sts_SeqIdle,CEMS10_Stack_Cal_Seq_Sts_SeqIdle,CEMS10_Stack_NH3_Cal_Seq_Sts_SeqIdle"
      url = "https://10.101.206.21/webapi/api/trend/multitag?tags=" + tags_to_request + "&start=" + @from_unix.to_s + "&end=" + @to_unix.to_s + "&interval=" + @interval.to_s
      puts url
      begin
          result = HTTParty.get(url, :verify => false)
          puts "  result: " + result.class.to_s
          if result["message"] != "SUCCESS" then
            puts "Oh oh!"
          end
      rescue Exception => e
          last_error = e
          puts e.message
      end
      result_parsed = JSON.parse(result.to_s)["data"]
      result_parsed = Hash.new
      result_parsed["CEMS10_Stack_Cal_Seq_Sts_SeqIdle"] = [
        {"T"=>1638381540000, "V"=>-1.0, "Q"=>11},
        {"T"=>1638381600000, "V"=>0.0, "Q"=>11},
        {"T"=>1638381660000, "V"=>0.0, "Q"=>11},
        {"T"=>1638381720000, "V"=>-1.0, "Q"=>11},
      ]
      result_parsed["CEMS10_Stack_NH3_Cal_Seq_Sts_SeqIdle"] = [
        {"T"=>1638381540000, "V"=>-1.0, "Q"=>11},
        {"T"=>1638381600000, "V"=>0.0, "Q"=>11},
        {"T"=>1638381660000, "V"=>0.0, "Q"=>11},
        {"T"=>1638381720000, "V"=>-1.0, "Q"=>11},
      ]
      result_parsed["CEMS10_SCR_Inlet_Cal_Seq_Sts_SeqIdle"] = [
        {"T"=>1638381540000, "V"=>-1.0, "Q"=>11},
        {"T"=>1638381600000, "V"=>0.0, "Q"=>11},
        {"T"=>1638381660000, "V"=>0.0, "Q"=>11},
        {"T"=>1638381720000, "V"=>-1.0, "Q"=>11},
      ]

      values_to_investigate = result_parsed["CEMS10_Stack_Cal_Seq_Sts_SeqIdle"].map{|element| element["V"]}

      transitions = []
      ndx_offset = 0
      transition_ndx = true
      while !transition_ndx.nil?
        transition_ndx = get_a_transition_from values_to_investigate
        transitions << (transition_ndx + ndx_offset) if !transition_ndx.nil?
        puts ndx_offset.to_s + " " + " " + transition_ndx.to_s + " " + (transition_ndx+ndx_offset).to_s + " " + values_to_investigate.to_s + " " + transitions.to_s if !transition_ndx.nil?

        values_to_investigate.shift(transition_ndx-1)
        ndx_offset = ndx_offset + 1
      end 
      return result_parsed
    end

    def get_a_transition_from values
      found = nil
      last_ndx = 0
      last_value = values[0]
      values.each_index do |ndx|
        if last_ndx == 0
          if values[ndx] != last_value
            last_ndx = ndx
            last_value = values[ndx]
          end
        end
      end
      found = last_ndx if last_ndx != 0
      return found
    end
end