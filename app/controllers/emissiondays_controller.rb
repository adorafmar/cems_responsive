class EmissiondaysController < ApplicationController

  def index

  end

  def show
    if valid? then
      normalize_time_range_needed
      @columns_human = [
        # ["Monitoring","CEMS10_MonitoringCode",nil],
        ["Process","CEMS10_ProcessCode",nil],
        ["Outlet NOx","CEMS10_Stack_NOx_EmRate_HDM_Daily","lb/day"],
        ["Outlet NOx Excess w/o Burner","outlet_nox_excess_without","lb/day > #{LIMIT_WITHOUT_BURNER_DAILY}"],
        ["Outlet NOx Excess with Burner","outlet_nox_excess_with","lb/day > #{LIMIT_WITH_BURNER_DAILY}"],
        ["Outlet NOx Energy","CEMS10_Stack_NOxMWh_Ratio_HDM_Daily","lb/MWhr"],
      ]
      get_from_prism
    else
      redirect_to action: "index"
    end
  end

  private
  
end
