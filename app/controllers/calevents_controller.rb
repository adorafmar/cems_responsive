class CaleventsController < ApplicationController

  SPAN_CONSTANTS = Hash[{
    "Stack CO" => 100,
    "Stack CO" => 100,
    "Stack NH3" => 100,
    "Stack NO" => 100,
    "Stack NO" => 100,
    "Stack NO2" => 100,
    "Stack O2" => 25
  }]

  def index
    
  end

  def show
    if valid? then
      normalize_time_range_needed
      @columns_human = [
        [nil,"CEMS10_Stack_Cal_Seq_Sts_SeqIdle",nil],
        [nil,"CEMS10_Stack_NH3_Cal_Seq_Sts_SeqIdle",nil],
        [nil,"CEMS10_SCR_Inlet_Cal_Seq_Sts_SeqIdle",nil],
        
        [nil,"CEMS10_Inlet_NO_Setup_Expected_Zero",nil],
        [nil,"CEMS10_Inlet_NO_Setup_Expected_Span",nil],
        [nil,"CEMS10_Inlet_NO_Setup_Zero_Actual",nil],
        [nil,"CEMS10_Inlet_NO_Setup_Span_Actual",nil],
        [nil,"CEMS10_Inlet_NO_Setup_Zero_Error",nil],
        [nil,"CEMS10_Inlet_NO_Setup_Span_Error",nil],
        
        [nil,"CEMS10_Inlet_NO2_Setup_Expected_Zero",nil],
        [nil,"CEMS10_Inlet_NO2_Setup_Expected_Span",nil],
        [nil,"CEMS10_Inlet_NO2_Setup_Zero_Actual",nil],
        [nil,"CEMS10_Inlet_NO2_Setup_Span_Actual",nil],
        [nil,"CEMS10_Inlet_NO2_Setup_Zero_Error",nil],
        [nil,"CEMS10_Inlet_NO2_Setup_Span_Error",nil],
        
        [nil,"CEMS10_NH3_Ana_Setup_Expected_Zero",nil],
        [nil,"CEMS10_NH3_Ana_Setup_Expected_Span",nil],
        [nil,"CEMS10_NH3_Ana_Setup_Zero_Actual",nil],
        [nil,"CEMS10_NH3_Ana_Setup_Span_Actual",nil],
        [nil,"CEMS10_NH3_Ana_Setup_Zero_Error",nil],
        [nil,"CEMS10_NH3_Ana_Setup_Span_Error",nil],
        
        [nil,"CEMS10_Stack_CO_Setup_Expected_Zero",nil],
        [nil,"CEMS10_Stack_CO_Setup_Expected_Span",nil],
        [nil,"CEMS10_Stack_CO_Setup_Zero_Actual",nil],
        [nil,"CEMS10_Stack_CO_Setup_Span_Actual",nil],
        [nil,"CEMS10_Stack_CO_Setup_Zero_Error",nil],
        [nil,"CEMS10_Stack_CO_Setup_Span_Error",nil],
        
        [nil,"CEMS10_Stack_NO_Setup_Expected_Zero",nil],
        [nil,"CEMS10_Stack_NO_Setup_Expected_Span",nil],
        [nil,"CEMS10_Stack_NO_Setup_Zero_Actual",nil],
        [nil,"CEMS10_Stack_NO_Setup_Span_Actual",nil],
        [nil,"CEMS10_Stack_NO_Setup_Zero_Error",nil],
        [nil,"CEMS10_Stack_NO_Setup_Span_Error",nil],
        
        [nil,"CEMS10_Stack_NO2_Setup_Expected_Zero",nil],
        [nil,"CEMS10_Stack_NO2_Setup_Expected_Span",nil],
        [nil,"CEMS10_Stack_NO2_Setup_Zero_Actual",nil],
        [nil,"CEMS10_Stack_NO2_Setup_Span_Actual",nil],
        [nil,"CEMS10_Stack_NO2_Setup_Zero_Error",nil],
        [nil,"CEMS10_Stack_NO2_Setup_Span_Error",nil],
        
        [nil,"CEMS10_Stack_O2_Setup_Expected_Zero",nil],
        [nil,"CEMS10_Stack_O2_Setup_Expected_Span",nil],
        [nil,"CEMS10_Stack_O2_Setup_Zero_Actual",nil],
        [nil,"CEMS10_Stack_O2_Setup_Span_Actual",nil],
        [nil,"CEMS10_Stack_O2_Setup_Zero_Error",nil],
        [nil,"CEMS10_Stack_O2_Setup_Span_Error",nil],
        
        [nil,"CEMS10_Stack_Analyzer_CO_5Day_AL",nil],
        [nil,"CEMS10_Stack_Analyzer_NO_5Day_AL",nil],
        [nil,"CEMS10_Stack_Analyzer_NO2_5Day_AL",nil],
        [nil,"CEMS10_Stack_Analyzer_O2_5Day_AL",nil],
      ]
      tags_analyzers = add_analyzers @columns_human.map{|record| record[1]}
      get_from_prism
      @records_normalized = normalize @records, -0.5
      calibration_signals = remove_spurious_from_all @records_normalized
      transitions = find_transitions_in_all_signals calibration_signals
      calibration_events = find_calibration_events_for_all_signals transitions
      values = values_for_all_calibration_events calibration_events
      @report_records = organize tags_analyzers, calibration_events, values
    else
      redirect_to action: "index"
    end
  end

  def organize tags_analyzers, calibration_events, values
    array_out = []
    calibration_events.each do |signal, down_ats|
      case signal
      when :calibration_signal_stack
        analyzers = ["Stack CO", "Stack NO", "Stack NO2", "Stack O2"]
      when :calibration_signal_nh3
        analyzers = ["NH3"]
      when :calibration_signal_inlet
        analyzers = ["Inlet NO", "Inlet NO2"]
      end
      analyzers.each do |analyzer| 
        down_ats.each do |down_at|
          puts "signal: " + signal.to_s + " analyzer: " + analyzer.to_s + " down: " + down_at.to_s
          if not down_at.nil?
            tag_expected_zero = tags_analyzers.select do |tag_analyzer| 
              tag_analyzer[:analyzer] == analyzer and tag_analyzer[:tag]["Expected"] and tag_analyzer[:tag]["Zero"]
            end
            tag_expected_span = tags_analyzers.select do |tag_analyzer| 
              tag_analyzer[:analyzer] == analyzer and tag_analyzer[:tag]["Expected"] and tag_analyzer[:tag]["Span"]
            end
            tag_actual_zero = tags_analyzers.select do |tag_analyzer| 
              tag_analyzer[:analyzer] == analyzer and tag_analyzer[:tag]["Actual"] and tag_analyzer[:tag]["Zero"]
            end
            tag_actual_span = tags_analyzers.select do |tag_analyzer| 
              tag_analyzer[:analyzer] == analyzer and tag_analyzer[:tag]["Actual"] and tag_analyzer[:tag]["Span"]
            end
            tag_error_zero = tags_analyzers.select do |tag_analyzer| 
              tag_analyzer[:analyzer] == analyzer and tag_analyzer[:tag]["Error"] and tag_analyzer[:tag]["Zero"]
            end
            tag_error_span = tags_analyzers.select do |tag_analyzer| 
              tag_analyzer[:analyzer] == analyzer and tag_analyzer[:tag]["Error"] and tag_analyzer[:tag]["Span"]
            end
            tag_5_day_limit = tags_analyzers.select do |tag_analyzer| 
              tag_analyzer[:analyzer] == analyzer and tag_analyzer[:tag]["5Day_AL"]
            end

            if not tag_expected_zero.empty?
              tag_expected_zero = tag_expected_zero[0][:tag]
            end
            if not tag_expected_span.empty?
              tag_expected_span = tag_expected_span[0][:tag]
            end
            if not tag_actual_zero.empty?
              tag_actual_zero = tag_actual_zero[0][:tag]
            end
            if not tag_actual_span.empty?
              tag_actual_span = tag_actual_span[0][:tag]
            end
            if not tag_error_zero.empty?
              tag_error_zero = tag_error_zero[0][:tag]
            end
            if not tag_error_span.empty?
              tag_error_span = tag_error_span[0][:tag]
            end
            if not tag_5_day_limit.empty?
              tag_5_day_limit = tag_5_day_limit[0][:tag]
            end
            
            ["Zero","Span"].each do |zs|
              expected_zero = values[signal].select do |record|
                record["DateTime"] == down_at
              end.first[tag_expected_zero]
              expected_span = values[signal].select do |record|
                record["DateTime"] == down_at
              end.first[tag_expected_span]
              actual_zero = values[signal].select do |record|
                record["DateTime"] == down_at
              end.first[tag_actual_zero]
              actual_span = values[signal].select do |record|
                record["DateTime"] == down_at
              end.first[tag_actual_span]
              error_zero = values[signal].select do |record|
                record["DateTime"] == down_at
              end.first[tag_error_zero]
              error_span = values[signal].select do |record|
                record["DateTime"] == down_at
              end.first[tag_error_span]

              expected_value = zs == "Zero" ? expected_zero : expected_span
              actual_value   = zs == "Zero" ? actual_zero : actual_span
              error_value    = zs == "Zero" ? error_zero : error_span

              expected_value = expected_value.round(2) unless expected_value.nil?
              actual_value   = actual_value.round(2) unless actual_value.nil?
              error_value    = error_value.round(2) unless error_value.nil?

              p60 = (error_value > 5 ? "Fail" : " Pass") unless error_value.nil?
              span = SPAN_CONSTANTS[analyzer]
              if tag_5_day_limit.class == Integer then
                five_day_count = values[signal][tag_5_day_limit]
              else
                five_day_count = ""
              end
              array_out << [
                Time.at(down_at),
                analyzer,
                zs,
                expected_value,
                actual_value,
                error_value,
                p60,
                five_day_count,
                span,
              ] 
            end
          end
        end
      end
    end
    array_out.sort! do |a,b|
      va = a[0].to_i + a[1].bytes.inject(:+) - a[2].bytes.inject(:+)
      vb = b[0].to_i + b[1].bytes.inject(:+) - b[2].bytes.inject(:+)
      va <=> vb
    end
    return array_out
  end

  def values_for_all_calibration_events calibration_signals_events
    values = {}
    calibration_signals_events.each do |signal, events|
      values[signal] = []
      events.each do |event|
        calibration_records = @records.select do |record|
          record["DateTime"] == event
        end
        values[signal] << calibration_records[0]
      end
    end
    return values
  end

  def find_calibration_events_for_all_signals transitions
    calibration_events = {}
    calibration_events[:calibration_signal_stack] = all_transitions_down transitions[:calibration_signal_stack]
    calibration_events[:calibration_signal_nh3]   = all_transitions_down transitions[:calibration_signal_nh3]
    calibration_events[:calibration_signal_inlet] = all_transitions_down transitions[:calibration_signal_inlet]
    return calibration_events
  end

  def all_transitions_down transitions
    occurred_at = []
    transitions.each do |transition| 
      when_occured = transition[0]
      up_direction = transition[1]
      if not up_direction
        occurred_at << when_occured
      else
      end
    end
    return occurred_at
  end

  def add_analyzers tags
    tags = tags.filter{|tag| not tag.downcase.include? "idle"}
    tags_analyzers = []
    fragments = ["CEMS10","Setup","Zero","Span","5Day","AL","Expected","Actual","Error","Alarm","Limit","Analyzer","Ana"]
    tags.each do |tag|
      analyzer = tag
      fragments.each do |fragment|
        analyzer = analyzer.remove(/#{fragment}/)
      end
      analyzer = analyzer.split("_").uniq
      analyzer.delete("")
      analyzer.delete([])
      analyzer = analyzer.join(" ")
      tags_analyzers << Hash[:analyzer => analyzer, :tag => tag]
    end
    return tags_analyzers
  end

  def find_transitions_in_all_signals calibration_signals
    transitions = {}
    transitions[:calibration_signal_stack] = find_transitions_in calibration_signals[:calibration_signal_stack]
    transitions[:calibration_signal_nh3]   = find_transitions_in calibration_signals[:calibration_signal_nh3]
    transitions[:calibration_signal_inlet] = find_transitions_in calibration_signals[:calibration_signal_inlet]
    return transitions
  end

  def find_transitions_in array_in
    time_previous = array_in.first[0]
    value_previous = array_in.first[1]
    array_out = []
    ndx = 1
    while ndx != array_in.length
      time_current = array_in[ndx][0]
      value_current = array_in[ndx][1]
      if value_current != value_previous
        direction = value_current and not value_previous
        array_out << [time_current, direction]
      else
      end
      time_previous = time_current
      value_previous = value_current
      ndx = ndx + 1
    end
    return array_out
  end

  def normalize array_in, threshold
    array_out = []
    return array_in.each do |element| 
      element["DateTime"] = element["DateTime"].to_i
      element["CEMS10_Stack_Cal_Seq_Sts_SeqIdle"] = element["CEMS10_Stack_Cal_Seq_Sts_SeqIdle"] > threshold ? true : false
      element["CEMS10_Stack_NH3_Cal_Seq_Sts_SeqIdle"] = element["CEMS10_Stack_NH3_Cal_Seq_Sts_SeqIdle"] > threshold ? true : false
      element["CEMS10_SCR_Inlet_Cal_Seq_Sts_SeqIdle"] = element["CEMS10_SCR_Inlet_Cal_Seq_Sts_SeqIdle"] > threshold ? true : false
      array_out << element
    end
    return array_out
  end

  def remove_spurious_from_all array_in
    css_cleaned = remove_spurious(array_in.map do |element|
      Hash[{when: element["DateTime"], level: element["CEMS10_Stack_Cal_Seq_Sts_SeqIdle"]}]
    end)
    csn_cleaned = remove_spurious(array_in.map do |element|
      Hash[{when: element["DateTime"], level: element["CEMS10_Stack_NH3_Cal_Seq_Sts_SeqIdle"]}]
    end)
    csi_cleaned = remove_spurious(array_in.map do |element|
      Hash[{when: element["DateTime"], level: element["CEMS10_SCR_Inlet_Cal_Seq_Sts_SeqIdle"]}]
    end)
    rv = Hash[{
      calibration_signal_stack: css_cleaned, 
      calibration_signal_nh3: csn_cleaned,
      calibration_signal_inlet: csi_cleaned
    }]
    return rv
  end

  def remove_spurious array_in
    left_value = false
    center_value = false
    right_value = false
    ndx = 1
    array_out = []
    while ndx != array_in.length - 1
      left_value = array_in[ndx-1][:level]
      center_value = array_in[ndx][:level]
      right_value = array_in[ndx+1][:level]
      array_out << [array_in[ndx][:when], ((left_value and center_value) or (center_value and right_value))]
      ndx = ndx + 1
    end
    return array_out
  end
  
end