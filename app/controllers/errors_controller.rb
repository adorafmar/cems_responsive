require 'pry'
require 'uri'
require 'httparty'
require 'json'

class ErrorsController < ApplicationController
  def index
    render file: "505.html"
  end
end
