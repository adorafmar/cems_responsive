require 'pry'
require 'uri'
require 'httparty'
require 'json'

class QuarterhourliesController < ApplicationController
  # before_action :set_minutely, only: %i[ show edit update destroy ]ß

  # GET /minutelies or /minutelies.json
  def index
    # @minutelies = Minutely.all
  end

  def convert_to_unix_time p_date_from
    p_from_month = Date::ABBR_MONTHNAMES.index(p_date_from.split(" ")[0])
    p_from_day = p_date_from.split(" ")[1].split(",")[0].to_i
    p_from_year = p_date_from.split(" ")[2].to_i
    p_from_hour = p_date_from.split(" ")[3].split(":")[0].to_i
    p_from_minute = p_date_from.split(":")[1].split(" ")[0].to_i
    p_from_meridian = p_date_from.split(" ")[4].split(":")[0]
    if p_from_meridian.downcase == "am" && p_from_hour == 12
      p_from_hour = p_from_hour - 12
    end
    if p_from_meridian.downcase == "pm" && p_from_hour != 12
      p_from_hour = p_from_hour + 12
    end
    return Time.new(p_from_year, p_from_month, p_from_day, p_from_hour, p_from_minute, 0, "-05:00").to_i*1000
  end

  def get_data
    progress = 0
    @from_unix = convert_to_unix_time @from_human
    @to_unix   = convert_to_unix_time @to_human
    days_needed = @to_unix - @from_unix
    puts "range: " + @from_unix.to_s + ", " + @from_unix.to_s
    while @from_unix < @to_unix do
        togo = days_needed - (@to_unix - @from_unix)
        puts "while: " + ((@to_unix - @from_unix)/1000/60/60/24).round(0).to_s
        progress = togo / days_needed
        url = "https://10.101.206.21/webapi/api/trend/multitag?tags=" + @tags_to_request + "&start=" + @from_unix.to_s + "&end=" + @to_unix.to_s + "&interval=" + @interval.to_s
        puts url
        begin
            result = HTTParty.get(url, :verify => false)
            if result["message"] != "SUCCESS" then
              puts "Oh oh!"
            end
        rescue Exception => e
            last_error = e
            puts e.message  
            # puts e.backtrace.inspect  
        end
        result_parsed = JSON.parse(result.to_s)["data"]
        puts result.inspect if result_parsed.nil?
        puts "result_parsed: " + result_parsed.class.to_s
        first_key = result_parsed.keys.first
        @from_unix = result_parsed[first_key][result_parsed[first_key].length-1]["T"]
        @responses << result_parsed
    end
    puts "range: " + @from_unix.to_s + ", " + @to_unix.to_s
    puts "@responses.length: " + @responses.length.to_s
  end

  # GET /minutelies/1 or /minutelies/1.json
  def show
    normalize_time_range_needed
    @columns_human = [
      ["Monitoring","CEMS10_MonitoringCode"],
      ["Process","CEMS10_ProcessCode"],
      ["Inlet Nox ppm","CEMS10_SCR_Inlet_Analyzer_NOx"],
      ["MC","CEMS10_SCR_Inlet_Analyzer_NOx_MC"],
      ["Inlet H2O ppm","CEMS10_SCR_Inlet_Analyzer_H2O"],
      ["MC","CEMS10_SCR_Inlet_Analyzer_H2O_MC"],
      ["Outlet Nox","CEMS10_Stack_Analyzer_NOx"],
      ["MC","CEMS10_Stack_Analyzer_NOx_MC"],
      ["Outlet CO ppm","CEMS10_Stack_Analyzer_CO"],
      ["MC","CEMS10_Stack_Analyzer_CO_MC"],
      ["Outlet H2O ppm","CEMS10_Stack_Analyzer_H2O"],
      ["MC","CEMS10_Stack_Analyzer_H2O_MC"],
      ["Outlet O2 %","CEMS10_Stack_Analyzer_O2"],
      ["MC","CEMS10_Stack_Analyzer_O2_MC"],
      ["Outlet NH3 ppm","CEMS10_Stack_NH3"],
      ["MC","CEMS10_NH3_Analyzer_MC"],
      ["Outlet Stk Pres inH2O","CEMS10_Stack_DP"],
      ["MC",""],
      ["Outlet Stk Temp degF","CEMS10_Stack_Temperature"],
      ["MC",""],
      ["U10 Stk Flow KSCFH?","CEMS10_Stack_Flow"],
      ["MC",""]
    ]
    @columns = []
    @columns_human.each do |column_human|
      @columns << column_human[1] if not column_human[1].empty?
    end
    last_error = nil
    @responses = Array.new
    columns_to_request = @columns[0 .. @columns.length]
    columns_to_request.delete_if {|column| column.empty?} 

    @tags_to_request = columns_to_request.shift(10)
    @tags_to_request = @tags_to_request.join(",")
    get_data

    @tags_to_request = columns_to_request.shift(10)
    @tags_to_request = @tags_to_request.join(",")
    get_data

    collected_columns = Hash.new
    response_i = 0
    @responses.each do |response|
      column_names = response.keys
      column_names.each do |column_name|
        if collected_columns[column_name].nil?
          collected_columns[column_name] = response[column_name]
        else
          collected_columns[column_name].concat(response[column_name])
        end
        # puts response_i.to_s + " " + column_name + " c: " + collected_columns[column_name].length.to_s + " r: " + response[column_name].length.to_s
      end
      response_i = response_i + 1
    end

    records_to_report = Array.new
    collected_column_names = collected_columns.keys
    n_rows = collected_columns.first.second.length
    collected_columns.each do |collected_column|
    end

    row_i = 0
    while row_i != n_rows
      if collected_columns[collected_column_names.first][row_i]["T"] < @to_unix
        record_to_report = Hash.new
        collected_column_names.each do |name|
          record_to_report["when"] = collected_columns[name][row_i]["T"]
          record_to_report[name]   = collected_columns[name][row_i]["V"]
        end
        records_to_report << record_to_report
      end
      row_i = row_i + 1
    end

    @aggregate_columns = @columns[0 .. @columns.length]
    @aggregate_columns.delete_if {|column| column.include? "Code" } 
    @aggregate_columns.delete_if {|column| column.include? "_MC" } 
    @records = Array.new
    @minimums = Hash.new
    @maximums = Hash.new
    @averages = Hash.new
    for aggregate_column in @aggregate_columns
        @averages[aggregate_column] = 0
    end

    first_key = records_to_report.first.keys.first
    for record_to_report in records_to_report do
        record = Hash.new
        begin
          record["DateTime"] = Time.at(record_to_report[first_key]/1000).to_datetime
        rescue
        end
        for column in @columns 
          record[column] = record_to_report[column]
        end
        @records << record
        for aggregate_column in @aggregate_columns 
            update_aggregates_for aggregate_column, record
        end
    end

    @records.map{ |record|
        case record["CEMS10_MonitoringCode"]
            when 0
                record["CEMS10_MonitoringCode"] = "Normal Operation"
            when 10
                record["CEMS10_MonitoringCode"] = "Required Adjustment Not Made"
            when 11
                record["CEMS10_MonitoringCode"] = "Excess Drift Primary Analyzer"
            when 12
                record["CEMS10_MonitoringCode"] = "Excess Drift Ancillary Analyzer"
            when 13
                record["CEMS10_MonitoringCode"] = "Process Down"
            when 14
                record["CEMS10_MonitoringCode"] = "Recalibration"
            when 15
                record["CEMS10_MonitoringCode"] = "Preventive Maintenance"
            when 16
                record["CEMS10_MonitoringCode"] = "Primary Analyzer Malfunction"
            when 17
                record["CEMS10_MonitoringCode"] = "Ancillary Analyzer Malfunction"
            when 18
                record["CEMS10_MonitoringCode"] = "Data Handling System Malfunction"
            when 19
                record["CEMS10_MonitoringCode"] = "Sample Interface Malfunction"
            when 20
                record["CEMS10_MonitoringCode"] = "Corrective Maintenance"
            when 21
                record["CEMS10_MonitoringCode"] = "Blowback"
            when 22
                record["CEMS10_MonitoringCode"] = "Analyzer Over/Under Range"
            when 23
                record["CEMS10_MonitoringCode"] = "Unit Not Being Sampled"
            when 98
                record["CEMS10_MonitoringCode"] = "Auto-Calibration"
            when 99
                record["CEMS10_MonitoringCode"] = "Software Maintenance"
        end
        case record["CEMS10_ProcessCode"]
            when 8
                record["CEMS10_ProcessCode"] = "Normal Operation"
            when 1
                record["CEMS10_ProcessCode"] = "Monitor Equipment Malfucntion"
            when 2
                record["CEMS10_ProcessCode"] = "Control Equipment Malfunction (Non-Monitor Equipmemt Malfunction)"
            when 3
                record["CEMS10_ProcessCode"] = "Starting Up (Quality Assurance Calibrations)"
            when 4
                record["CEMS10_ProcessCode"] = "Shutting Down (Other Known Causes)"
            when 5
                record["CEMS10_ProcessCode"] = "Unit is Offline"
            when 6
                record["CEMS10_ProcessCode"] = "Clean Process Equipment"
            when 7
                record["CEMS10_ProcessCode"] = "Cleaning Equipment"
        end
    }

    for aggregate_column in @aggregate_columns
      @averages[aggregate_column] = @averages[aggregate_column] / records_to_report.length
    end
    headers = @columns_human.map {|row| row[1]}
    CSV.open(Rails.root.join('public',"report.csv").to_s, "w") do |csv|
        csv << headers
        @records.each do |record|
          csv << record.values
        end
    end
  end

  # GET /minutelies/new
  def new
    # @minutely = Minutely.new
  end

  # GET /minutelies/1/edit
  def edit
  end

  # POST /minutelies or /minutelies.json
  def create
    @minutely = Minutely.new(minutely_params)

    respond_to do |format|
      if @minutely.save
        format.html { redirect_to @minutely, notice: "Minutely was successfully created." }
        format.json { render :show, status: :created, location: @minutely }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @minutely.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /minutelies/1 or /minutelies/1.json
  def update
    respond_to do |format|
      if @minutely.update(minutely_params)
        format.html { redirect_to @minutely, notice: "Minutely was successfully updated." }
        format.json { render :show, status: :ok, location: @minutely }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @minutely.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /minutelies/1 or /minutelies/1.json
  def destroy
    @minutely.destroy
    respond_to do |format|
      format.html { redirect_to minutelies_url, notice: "Minutely was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    def update_aggregates_for column, record
      begin
        @minimums[column] =    record[column] if @minimums[column].nil?
        @maximums[column] =    record[column] if @maximums[column].nil?
        @minimums[column] =    record[column] < @minimums[column] ? record[column] : @minimums[column]
        @maximums[column] =    record[column] > @maximums[column] ? record[column] : @maximums[column]
        @averages[column] = @averages[column] +    record[column]
      rescue
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_minutely
      @minutely = Minutely.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def minutely_params
      params.require(:minutely).permit(:from, :to)
    end
end
