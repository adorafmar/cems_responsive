class MonthliesController < ApplicationController

  def index

  end

  def show
      normalize_time_range_needed
      @columns_human = [
        # ["Monitoring","CEMS10_MonitoringCode",nil],
        ["Process","CEMS10_ProcessCode",nil],
        ["Outlet CO","CEMS10_Stack_Analyzer_CO_HDM_CM","ppm"],
        ["Outlet NO","CEMS10_Stack_Analyzer_NO_HDM_CM","ppm"],
        ["Outlet NO2","CEMS10_Stack_Analyzer_NO2_HDM_CM","ppm"],
        ["Outlet NOx","CEMS10_Stack_Analyzer_NOx_HDM_CM","ppm"],
        ["Outlet O2","CEMS10_Stack_Analyzer_O2_HDM_CM","%"],
        ["Outlet NH3","CEMS10_NH3_Analyzer_HDM_CM","ppm"],
        ["Inlet NO","CEMS10_SCR_Inlet_Analyzer_NO_HDM_CM","ppm"],
        ["Inlet NO2","CEMS10_SCR_Inlet_Analyzer_NO2_HDM_CM","ppm"],
        ["Inlet NOx","CEMS10_SCR_Inlet_Analyzer_NOx_HDM_CM","ppm"],
        ["CO Emission Rate","CEMS10_CO_Emission_Rate","lb/hr"],
        ["NOx Emission Rate","CEMS10_NOx_Emission_Rate","lb/hr"],
        ["Temperature","CEMS10_Stack_Temperature","F"],
        ["Flow","CEMS10_Stack_Flow","KSCFH"],
        ["Pressue","CEMS10_Stack_DP","KSCFH"],
      ]
      get_from_prism
  end

  private
  
end
