require 'pry'
require 'uri'
require 'httparty'
require 'json'

class RangesController < ApplicationController
  before_action :set_range, only: %i[ show edit update destroy ]

  # GET /ranges or /ranges.json
  def index
    # @ranges = Range.all
  end

  # GET /ranges/1 or /ranges/1.json
  def show
    normalize_time_range_needed
  end

  # GET /ranges/new
  def new
    tags_to_request = "CEMS10_MonitoringCode,CEMS10_ProcessCode,"

    puts "executing go"
    last_error = nil
    progress = 0
    complete = false
    from_unix = DateTime.parse(request.GET["from"]).to_i*1000
    to_unix = DateTime.parse(request.GET["to"]).to_i*1000
    days_needed = to_unix - from_unix
    puts "range: " + from_unix.to_s + ", " + from_unix.to_s
    data_received = Array.new
    while from_unix < to_unix do
        togo = days_needed - (to_unix - from_unix)
        puts "while: " + ((to_unix - from_unix)/1000/60/60/24).round(0).to_s
        progress = togo / days_needed
        begin
            url = "https://10.101.206.21/webapi/api/trend/multitag?tags=" + tags_to_request + "&start=" + from_unix.to_s + "&end=" + to_unix.to_s + "&interval=60000"
            puts url
            result = HTTParty.get(url, :verify => false)
            if result["message"] != "SUCCESS" then
              puts "Oh oh!"
            end
        rescue Exception => e
            last_error = e
            puts e.message  
            # puts e.backtrace.inspect  
        end
        result_parsed = JSON.parse(result.to_s)["data"]
        first_key = result_parsed.keys[0]
        from_unix = result_parsed[first_key][result_parsed[first_key].length-1]["T"]
        data_received << result_parsed
    end
    puts "range: " + from_unix.to_s + ", " + to_unix.to_s
    puts "data_received.length: " + data_received.length.to_s
    records_to_report = Array.new
    columns = data_received[0].keys
    n_rows = data_received[0][columns[0]].length
    for response in data_received do
        row_i = 0
        while row_i != n_rows do
            record = Hash.new
            for column in columns do
              record[column] = response[column][row_i]
            end
            records_to_report << record
            row_i = row_i + 1
        end
    end
    if records_to_report.length == 0 then
        complete = true
    else
        time_prev = records_to_report[0]["CEMS10_MonitoringCode"]["T"]
        state_prev = records_to_report[0]["CEMS10_MonitoringCode"]["V"]
        @records = Array.new
        records_to_report_i = 1
        while records_to_report_i != records_to_report.length do
            if records_to_report_i != 0 then
                element = records_to_report[records_to_report_i]["CEMS10_MonitoringCode"]
                if element.nil? then
                    break
                else
                    process_code = records_to_report[records_to_report_i]["CEMS10_ProcessCode"]["V"]
                    time_next = element["T"]
                    state_next = element["V"]
                    if state_next != state_prev then
                        record = Hash.new
                        record["start"] = time_prev
                        record["stop"] = time_next
                        record["duration"] = time_next - time_prev
                        record["state"] = state_next
                        record["process_code"] = process_code
                        @records << record
                        state_prev = state_next
                        time_prev = time_next
                    end
                end
            end
            records_to_report_i = records_to_report_i + 1
        end
        ordinal = 1
        @records.map{ |record|
            # ordinal = ordinal
            # start = start
            # stop = stop
            record["duration"] = (record["duration"]/1000/60/60).round(1)
            case record["CEMS10_MonitoringCode"]
                when 0
                    record["state"] = "Normal Operation"
                when 10
                    record["state"] = "Required Adjustment Not Made"
                when 11
                    record["state"] = "Excess Drift Primary Analyzer"
                when 12
                    record["state"] = "Excess Drift Ancillary Analyzer"
                when 13
                    record["state"] = "Process Down"
                when 14
                    record["state"] = "Recalibration"
                when 15
                    record["state"] = "Preventive Maintenance"
                when 16
                    record["state"] = "Primary Analyzer Malfunction"
                when 17
                    record["state"] = "Ancillary Analyzer Malfunction"
                when 18
                    record["state"] = "Data Handling System Malfunction"
                when 19
                    record["state"] = "Sample Interface Malfunction"
                when 20
                    record["state"] = "Corrective Maintenance"
                when 21
                    record["state"] = "Blowback"
                when 22
                    record["state"] = "Analyzer Over/Under Range"
                when 23
                    record["state"] = "Unit Not Being Sampled"
                when 98
                    record["state"] = "Auto-Calibration"
                when 99
                    record["state"] = "Software Maintenance"
                else
                    record["state"] = "(not defined)"
            end
            case record["process_code"]
                when 8
                    record["process_code"] = "Normal Operation"
                when 1
                    record["process_code"] = "Monitor Equipment Malfucntion"
                when 2
                    record["process_code"] = "Control Equipment Malfunction (Non-Monitor Equipmemt Malfunction)"
                when 3
                    record["process_code"] = "Starting Up (Quality Assurance Calibrations)"
                when 4
                    record["process_code"] = "Shutting Down (Other Known Causes)"
                when 5
                    record["process_code"] = "Unit is Offline"
                when 6
                    record["process_code"] = "Clean Process Equipment"
                when 7
                    record["process_code"] = "Cleaning Equipment"
                else
                    record["process_code"] = "(not defined)"
            end
            ordinal = ordinal + 1
        }
        complete = true
    end
  end

  # GET /ranges/1/edit
  def edit
  end

  # POST /ranges or /ranges.json
  def create
    # @range = Range.new(range_params)

    # respond_to do |format|
    #   if @range.save
    #     format.html { redirect_to @range, notice: "Range was successfully created." }
    #     format.json { render :show, status: :created, location: @range }
    #   else
    #     format.html { render :new, status: :unprocessable_entity }
    #     format.json { render json: @range.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /ranges/1 or /ranges/1.json
  def update
    respond_to do |format|
      if @range.update(range_params)
        format.html { redirect_to @range, notice: "Range was successfully updated." }
        format.json { render :show, status: :ok, location: @range }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @range.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ranges/1 or /ranges/1.json
  def destroy
    @range.destroy
    respond_to do |format|
      format.html { redirect_to ranges_url, notice: "Range was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_range
      @range = Range.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def range_params
      params.require(:range).permit(:from, :to)
    end
end
