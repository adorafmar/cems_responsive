class EmissionmonthsController < ApplicationController
  
  def index

  end

  def show
    if valid? then
      normalize_time_range_needed
      @columns_human = [
        ["Process","CEMS10_ProcessCode",nil],
        ["Outlet NOx","CEMS10_Stack_NOx_EmRate_HDM_Monthly","lb/month"],
        ["Outlet NOx Excess w/o Burner","outlet_nox_excess_without","lb/month > #{LIMIT_WITHOUT_BURNER_MONTHLY}"],
        ["Outlet NOx Excess with Burner","outlet_nox_excess_with","lb/month > #{LIMIT_WITH_BURNER_MONTHLY}"],
        ["Outlet NOx Energy","CEMS10_Stack_NOxMWh_Ratio_HDM_Monthly","lb/MWhr"],
      ]
      get_from_prism
    else
      redirect_to action: "index"
    end
  end
  
end