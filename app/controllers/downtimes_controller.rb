require 'pry'
require 'uri'
require 'httparty'
require 'json'
require 'rails'

class DowntimesController < ApplicationController

  def index
    # logger.debug "more debug <<<<<<<<<<<<<<<<<<<<<<<"
    # logger.info "more info <<<<<<<<<<<<<<<<<<<<<<<"
    # logger.warn "more warn <<<<<<<<<<<<<<<<<<<<<<<"
    # logger.error "more error <<<<<<<<<<<<<<<<<<<<<<<"
    # logger.fatal "more fatal <<<<<<<<<<<<<<<<<<<<<<<"
  end

  def show
    if valid? then
      normalize_time_range_needed

      @columns_human = [
        ["Reason","CEMS10_MonitoringCode"],
        ["Category","CEMS10_ProcessCode"]
      ]

      @columns = []
      @columns_human.each do |column_human|
        @columns << column_human[1] if not column_human[1].empty?
      end
      last_error = nil
      @responses = Array.new
      columns_to_request = @columns[0 .. @columns.length]
      columns_to_request.delete_if {|column| column.empty?} 

      @tags_to_request = columns_to_request.shift(10)
      @tags_to_request = @tags_to_request.join(",")
      get_data

      collected_columns = Hash.new
      response_i = 0
      @responses.each do |response|
        column_names = response.keys
        column_names.each do |column_name|
          if collected_columns[column_name].nil?
            collected_columns[column_name] = response[column_name]
          else
            collected_columns[column_name].concat(response[column_name])
          end
        end
        response_i = response_i + 1
      end
      puts "responses received: " + @responses.length.to_s
      puts "columns found: " + collected_columns.length.to_s
      records_to_report = Array.new
      collected_column_names = collected_columns.keys
      n_rows = collected_columns.first.second.length
      collected_columns.each do |collected_column|
      end

      row_i = 0
      while row_i != n_rows
        if collected_columns[collected_column_names.first][row_i]["T"] < @to_unix
          record_to_report = Hash.new
          collected_column_names.each do |name|
            record_to_report["when_"] = collected_columns[name][row_i]["T"]
            record_to_report[name]   = collected_columns[name][row_i]["V"]
          end
          records_to_report << record_to_report
        end
        row_i = row_i + 1
      end
      puts "records_to_report: " + records_to_report.length.to_s


      @aggregate_columns = @columns[0 .. @columns.length]
      @aggregate_columns.delete_if {|column| column.include? "Code" } 
      @aggregate_columns.delete_if {|column| column.include? "_MC" } 
      @records = Array.new
      @minimums = Hash.new
      @maximums = Hash.new
      @averages = Hash.new

      first_key = records_to_report.first.keys.first
      for record_to_report in records_to_report do
          record = Hash.new
          begin
            record["when_"] = Time.at(record_to_report[first_key]/1000).to_datetime
          rescue
          end
          for column in @columns 
            record[column] = record_to_report[column]
          end
          @records << record
          for aggregate_column in @aggregate_columns 
              update_aggregates_for aggregate_column, record
          end
      end
      # tabular JSON
      @records.map{ |record|
          case record["CEMS10_MonitoringCode"]
              when 0
                  record["CEMS10_MonitoringCode"] = "Normal Operation"
              when 10
                  record["CEMS10_MonitoringCode"] = "Required Adjustment Not Made"
              when 11
                  record["CEMS10_MonitoringCode"] = "Excess Drift Primary Analyzer"
              when 12
                  record["CEMS10_MonitoringCode"] = "Excess Drift Ancillary Analyzer"
              when 13
                  record["CEMS10_MonitoringCode"] = "Process Down"
              when 14
                  record["CEMS10_MonitoringCode"] = "Recalibration"
              when 15
                  record["CEMS10_MonitoringCode"] = "Preventive Maintenance"
              when 16
                  record["CEMS10_MonitoringCode"] = "Primary Analyzer Malfunction"
              when 17
                  record["CEMS10_MonitoringCode"] = "Ancillary Analyzer Malfunction"
              when 18
                  record["CEMS10_MonitoringCode"] = "Data Handling System Malfunction"
              when 19
                  record["CEMS10_MonitoringCode"] = "Sample Interface Malfunction"
              when 20
                  record["CEMS10_MonitoringCode"] = "Corrective Maintenance"
              when 21
                  record["CEMS10_MonitoringCode"] = "Blowback"
              when 22
                  record["CEMS10_MonitoringCode"] = "Analyzer Over/Under Range"
              when 23
                  record["CEMS10_MonitoringCode"] = "Unit Not Being Sampled"
              when 98
                  record["CEMS10_MonitoringCode"] = "Auto-Calibration"
              when 99
                  record["CEMS10_MonitoringCode"] = "Software Maintenance"
          end
          case record["CEMS10_ProcessCode"]
              when 8
                  record["CEMS10_ProcessCode"] = "Normal Operation"
              when 1
                  record["CEMS10_ProcessCode"] = "Monitor Equipment Malfucntion"
              when 2
                  record["CEMS10_ProcessCode"] = "Control Equipment Malfunction (Non-Monitor Equipmemt Malfunction)"
              when 3
                  record["CEMS10_ProcessCode"] = "Starting Up (Quality Assurance Calibrations)"
              when 4
                  record["CEMS10_ProcessCode"] = "Shutting Down (Other Known Causes)"
              when 5
                  record["CEMS10_ProcessCode"] = "Unit is Offline"
              when 6
                  record["CEMS10_ProcessCode"] = "Clean Process Equipment"
              when 7
                  record["CEMS10_ProcessCode"] = "Cleaning Equipment"
          end
      }
      for aggregate_column in @aggregate_columns
        @averages[aggregate_column] = @averages[aggregate_column] / records_to_report.length
      end

      puts "records: " + @records.length.to_s
      if @records.length != 0
        time0 = @records.first["when_"]
        state0 = @records.first["CEMS10_MonitoringCode"]
        record_i = 0
        @aggregated_records = Array.new
        while record_i != @records.length
          time1 = @records[record_i]["when_"]
          state1 = @records[record_i]["CEMS10_MonitoringCode"]
          if state1 != state0
            @records[record_i]["when_previous"] = time0
            @records[record_i]["duration"] = (time1.to_time - time0.to_time)/60
            @aggregated_records << @records[record_i] unless state1.downcase.include? "normal"
            time0 = time1
            state0 = state1
          end
          record_i = record_i + 1
        end
      end
      puts "aggregated_records: " + @aggregated_records.length.to_s
      headers = @columns_human.map {|row| row[1]}
      CSV.open(Rails.root.join('public',"report.csv").to_s, "w") do |csv|
          csv << headers
          @records.each do |record|
            csv << record.values
          end
      end
    else
      redirect_to action: "index"
    end
  end

  # def get_data
  #   @@progress = 0
  #   days_needed = @to_unix - @from_unix
  #   logger.debug "get_data:range: " + @from_unix.to_s + ", " + @to_unix.to_s
  #   while @from_unix < @to_unix do
  #       togo = days_needed - (@to_unix - @from_unix)
  #       logger.debug "get_data:while: " + ((@to_unix - @from_unix)/1000/60/60/24).round(0).to_s
  #       @progress = togo.to_f / days_needed.to_f
  #       logger.debug "get_data:get_data:progress: " + @progress.to_s
  #       url = "https://10.101.206.21/webapi/api/trend/multitag?tags=" + @tags_to_request + "&start=" + @from_unix.to_s + "&end=" + @to_unix.to_s + "&interval=" + @interval.to_s
  #       logger.debug url
  #       begin
  #           result = HTTParty.get(url, :verify => false)
  #           if result["message"] != "SUCCESS" then
  #             logger.debug "get_data:Oh oh!"
  #           end
  #       rescue Exception => e
  #           last_error = e
  #           logger.debug e.message  
  #           # logger.debug e.backtrace.inspect  
  #       end
  #       result_parsed = JSON.parse(result.to_s)["data"]
  #       first_key = result_parsed.keys.first
  #       @from_unix = result_parsed[first_key][result_parsed[first_key].length-1]["T"]
  #       @responses << result_parsed
  #   end
  #   logger.debug "get_data:range: " + @from_unix.to_s + ", " + @to_unix.to_s
  #   logger.debug "get_data:@responses.length: " + @responses.length.to_s
  # end

  private
    def tag_exist? tag
      url = "https://10.101.206.21/webapi/api/trend/multitag?tags=" + tag + "&start=" + @from_unix.to_s + "&end=" + @to_unix.to_s + "&interval=" + @interval.to_s
      puts url
      begin
        result = HTTParty.get(url, :verify => false)
        if result["status"] == -1
          puts "bad tag: " + tag
          return false
        else
          puts "good tag: " + tag
          return true
        end
      rescue Exception => e
        puts "bad tag: " + tag
        return false
      end
      return true
    end

    def update_aggregates_for column, record
      @minimums[column] = record[column] if @minimums[column].nil?
      @maximums[column] = record[column] if @maximums[column].nil?
      @minimums[column] = record[column] < @minimums[column] ? record[column] : @minimums[column] unless record[column].nil? or @minimums[column].nil?
      @maximums[column] = record[column] > @maximums[column] ? record[column] : @maximums[column] unless record[column].nil? or @maximums[column].nil?
      @averages[column] = @averages[column] + record[column] unless record[column].nil? or @averages[column].nil?
    end
end
