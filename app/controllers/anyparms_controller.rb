class AnyparmsController < ApplicationController

  def index

  end

  def show
    if valid? then
      normalize_time_range_needed
      @columns_human = [
        ["Monitoring","CEMS10_MonitoringCode",nil],
        ["Process","CEMS10_ProcessCode",nil],
        ["Outlet CO","CEMS10_Stack_Analyzer_CO","ppm"],
        ["Outlet NO","CEMS10_Stack_Analyzer_NO","ppm"],
        ["Outlet NO2","CEMS10_Stack_Analyzer_NO2","ppm"],
        ["Outlet NOx","CEMS10_Stack_Analyzer_NOx","ppm"],
        ["Outlet O2","CEMS10_Stack_Analyzer_O2","%"],
        ["Outlet NH3","CTG10_CEMS_Outlet_NH3_Instantaneous","ppm"],
        ["Inlet NO","CEMS10_SCR_Inlet_Analyzer_NO","ppm"],
        ["Inlet NO2","CEMS10_SCR_Inlet_Analyzer_NO2","ppm"],
        ["Inlet NOx","CEMS10_SCR_Inlet_Analyzer_NOx","ppm"]
      ]
      @columns = []
      @columns_human.each do |column_human|
        @columns << column_human[1] if not column_human[1].empty?
      end
      last_error = nil
      @responses = Array.new
      columns_to_request = @columns[0 .. @columns.length]
      columns_to_request.delete_if {|column| column.empty?} 

      puts "looping"
      while columns_to_request.length != 0
        puts "  columns_to_request.length: " + columns_to_request.length.to_s
        @tags_to_request = columns_to_request.shift(10)
        eliminate_bad_tags
        @tags_to_request = @tags_to_request.join(",")
        puts "  tags_to_request: " + @tags_to_request
        get_data
      end

      collected_columns = Hash.new
      response_i = 0
      @responses.each do |response|
        column_names = response.keys
        column_names.each do |column_name|
          if collected_columns[column_name].nil?
            collected_columns[column_name] = response[column_name]
          else
            collected_columns[column_name].concat(response[column_name])
          end
          # puts response_i.to_s + " " + column_name + " c: " + collected_columns[column_name].length.to_s + " r: " + response[column_name].length.to_s
        end
        response_i = response_i + 1
      end

      records_to_report = Array.new
      collected_column_names = collected_columns.keys
      n_rows = collected_columns.first.second.length
      collected_columns.each do |collected_column|
      end

      row_i = 0
      while row_i != n_rows
        if collected_columns[collected_column_names.first][row_i]["T"] < @to_unix
          record_to_report = Hash.new
          collected_column_names.each do |name|
            record_to_report["when"] = collected_columns[name][row_i]["T"]
            record_to_report[name]   = collected_columns[name][row_i]["V"]
          end
          records_to_report << record_to_report
        end
        row_i = row_i + 1
      end

      @aggregate_columns = @columns[0 .. @columns.length]
      @aggregate_columns.delete_if {|column| column.include? "Code" } 
      @aggregate_columns.delete_if {|column| column.include? "_MC" } 
      @records = Array.new
      @minimums = Hash.new
      @maximums = Hash.new
      @averages = Hash.new
      for aggregate_column in @aggregate_columns
          @averages[aggregate_column] = 0
      end

      first_key = records_to_report.first.keys.first
      for record_to_report in records_to_report do
          record = Hash.new
          begin
            record["DateTime"] = Time.at(record_to_report[first_key]/1000).to_datetime
          rescue
          end

          for column in @columns
            record[column] = record_to_report[column]
          end
          @records << record
          for aggregate_column in @aggregate_columns 
              update_aggregates_for aggregate_column, record
          end
      end

      @records.map{ |record|
          case record["CEMS10_MonitoringCode"]
              when 0
                  record["CEMS10_MonitoringCode"] = "Normal Operation"
              when 10
                  record["CEMS10_MonitoringCode"] = "Required Adjustment Not Made"
              when 11
                  record["CEMS10_MonitoringCode"] = "Excess Drift Primary Analyzer"
              when 12
                  record["CEMS10_MonitoringCode"] = "Excess Drift Ancillary Analyzer"
              when 13
                  record["CEMS10_MonitoringCode"] = "Process Down"
              when 14
                  record["CEMS10_MonitoringCode"] = "Recalibration"
              when 15
                  record["CEMS10_MonitoringCode"] = "Preventive Maintenance"
              when 16
                  record["CEMS10_MonitoringCode"] = "Primary Analyzer Malfunction"
              when 17
                  record["CEMS10_MonitoringCode"] = "Ancillary Analyzer Malfunction"
              when 18
                  record["CEMS10_MonitoringCode"] = "Data Handling System Malfunction"
              when 19
                  record["CEMS10_MonitoringCode"] = "Sample Interface Malfunction"
              when 20
                  record["CEMS10_MonitoringCode"] = "Corrective Maintenance"
              when 21
                  record["CEMS10_MonitoringCode"] = "Blowback"
              when 22
                  record["CEMS10_MonitoringCode"] = "Analyzer Over/Under Range"
              when 23
                  record["CEMS10_MonitoringCode"] = "Unit Not Being Sampled"
              when 98
                  record["CEMS10_MonitoringCode"] = "Auto-Calibration"
              when 99
                  record["CEMS10_MonitoringCode"] = "Software Maintenance"
          end
          case record["CEMS10_ProcessCode"]
              when 8
                  record["CEMS10_ProcessCode"] = "Normal Operation"
              when 1
                  record["CEMS10_ProcessCode"] = "Monitor Equipment Malfucntion"
              when 2
                  record["CEMS10_ProcessCode"] = "Control Equipment Malfunction (Non-Monitor Equipmemt Malfunction)"
              when 3
                  record["CEMS10_ProcessCode"] = "Starting Up (Quality Assurance Calibrations)"
              when 4
                  record["CEMS10_ProcessCode"] = "Shutting Down (Other Known Causes)"
              when 5
                  record["CEMS10_ProcessCode"] = "Unit is Offline"
              when 6
                  record["CEMS10_ProcessCode"] = "Clean Process Equipment"
              when 7
                  record["CEMS10_ProcessCode"] = "Cleaning Equipment"
          end
      }
      for aggregate_column in @aggregate_columns
        @averages[aggregate_column] = @averages[aggregate_column] / records_to_report.length
      end
      headers = @columns_human.map {|row| row[1]}
      CSV.open(Rails.root.join('public',"report.csv").to_s, "w") do |csv|
          csv << headers
          @records.each do |record|
            csv << record.values
          end
      end
    else
      redirect_to action: "index"
    end
  end

  # GET /anyparms/new
  def new
    @anyparm = Anyparm.new
  end

  # GET /anyparms/1/edit
  def edit
  end

  # POST /anyparms or /anyparms.json
  def create
    @anyparm = Anyparm.new(anyparm_params)

    respond_to do |format|
      if @anyparm.save
        format.html { redirect_to @anyparm, notice: "Anyparm was successfully created." }
        format.json { render :show, status: :created, location: @anyparm }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @anyparm.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /anyparms/1 or /anyparms/1.json
  def update
    respond_to do |format|
      if @anyparm.update(anyparm_params)
        format.html { redirect_to @anyparm, notice: "Anyparm was successfully updated." }
        format.json { render :show, status: :ok, location: @anyparm }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @anyparm.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /anyparms/1 or /anyparms/1.json
  def destroy
    @anyparm.destroy
    respond_to do |format|
      format.html { redirect_to anyparms_url, notice: "Anyparm was successfully destroyed." }
      format.json { head :no_content }
    end
  end

end
