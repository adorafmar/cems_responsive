class MinuteliesController < ApplicationController

	def index

	end

	def show
    	if valid? then
	    	normalize_time_range_needed
		    @columns_human = [
		      # ["Monitoring","CEMS10_MonitoringCode",nil],
		      ["Process","CEMS10_ProcessCode",nil],
		      ["Outlet CO","CEMS10_Stack_Analyzer_CO_HDM_Hourly","ppm"],
		      ["Outlet NO","CEMS10_Stack_Analyzer_NO_HDM_Hourly","ppm"],
		      ["Outlet NO2","CEMS10_Stack_Analyzer_NO2_HDM_Hourly","ppm"],
		      ["Outlet NOx","CEMS10_Stack_Analyzer_NOx_HDM_Hourly","ppm"],
		      ["Outlet O2","CEMS10_Stack_Analyzer_O2_HDM_Hourly","%"],
		      ["Outlet NH3","CEMS10_NH3_Analyzer_HDM_Hourly","ppm"],
		      ["Inlet NO","CEMS10_SCR_Inlet_Analyzer_NO_HDM_Hourly","ppm"],
		      ["Inlet NO2","CEMS10_SCR_Inlet_Analyzer_NO2_HDM_Hourly","ppm"],
		      ["Inlet NOx","CEMS10_SCR_Inlet_Analyzer_NOx_HDM_Hourly","ppm"],
		      ["CO Emission Rate","CEMS10_CO_Emission_Rate","lb/hr"],
		      ["NOx Emission Rate","CEMS10_NOx_Emission_Rate","lb/hr"],
		      ["Temperature","CEMS10_Stack_Temperature","F"],
		      ["Flow","CEMS10_Stack_Flow","KSCFH"],
		      ["Pressue","CEMS10_Stack_DP","KSCFH"],
		    ]
	    	get_from_prism
	    else
	      redirect_to action: "index"
	    end
	end

	private
  
end
