require 'pry'
require 'uri'
require 'httparty'
require 'json'
require 'rails'
require 'active_support/core_ext'
require 'csv'

class ApplicationController < ActionController::Base
	@@progress = "initialized"

	LIMIT_WITHOUT_BURNER_HOURLY = 4.9
	LIMIT_WITH_BURNER_HOURLY = 7.1
	LIMIT_WITHOUT_BURNER_DAILY = 24 * LIMIT_WITHOUT_BURNER_HOURLY
	LIMIT_WITH_BURNER_DAILY = 24 * LIMIT_WITH_BURNER_HOURLY
	LIMIT_WITHOUT_BURNER_MONTHLY = 30 * LIMIT_WITHOUT_BURNER_DAILY
	LIMIT_WITH_BURNER_MONTHLY = 30 * LIMIT_WITH_BURNER_DAILY
	LIMIT_WITHOUT_BURNER_HOURLY = LIMIT_WITHOUT_BURNER_HOURLY.round(1)
	LIMIT_WITH_BURNER_HOURLY = LIMIT_WITH_BURNER_HOURLY.round(1)
	LIMIT_WITHOUT_BURNER_DAILY = LIMIT_WITHOUT_BURNER_DAILY.round(0)
	LIMIT_WITH_BURNER_DAILY = LIMIT_WITH_BURNER_DAILY.round(0)
	LIMIT_WITHOUT_BURNER_MONTHLY = LIMIT_WITHOUT_BURNER_MONTHLY.round(0)
	LIMIT_WITH_BURNER_MONTHLY = LIMIT_WITH_BURNER_MONTHLY.round(0)

	def initilize

	end

	def valid? 
		rc = true
		rc = (rc and not params["from"].blank?)
		rc = (rc and not params["fromt"].blank?)
		rc = (rc and not params["to"].blank?)
		rc = (rc and not params["tot"].blank?)
		return rc
	end

	def progress
		# @progress = Time.now
		puts "@@progress: " + @@progress.to_s
		@@progress = @@progress.to_s # (@@@progress * 100).round(0)
	end

	def get_from_prism
    	normalize_time_range_needed
	    @columns = []
	    @columns_human.each do |column_human|
	      @columns << column_human[1] if not column_human[1].empty?
	    end
	    last_error = nil
	    @responses = Array.new
	    columns_to_request = @columns[0 .. @columns.length]
	    columns_to_request.delete_if {|column| column.empty?} 

	    while columns_to_request.length != 0
			puts "columns_to_request.length: " + columns_to_request.length.to_s
			@tags_to_request = columns_to_request.shift(10)
			eliminate_bad_tags
			@tags_to_request = @tags_to_request.join(",")
			puts "  tags_to_request: " + @tags_to_request
			get_data
			puts @responses.length
	    end

	    collected_columns = Hash.new
	    response_i = 0
	    @responses.each do |response|
	      column_names = response.keys
	      column_names.each do |column_name|
	        if collected_columns[column_name].nil?
	          collected_columns[column_name] = response[column_name]
	        else
	          collected_columns[column_name].concat(response[column_name])
	        end
	        # puts response_i.to_s + " " + column_name + " c: " + collected_columns[column_name].length.to_s + " r: " + response[column_name].length.to_s
	      end
	      response_i = response_i + 1
	    end

	    records_to_report = Array.new
	    collected_column_names = collected_columns.keys
	    n_rows = collected_columns.first.second.length

	    row_i = 0
	    while row_i != n_rows
	      if collected_columns[collected_column_names.first][row_i]["T"] < @to_unix
	        record_to_report = Hash.new
	        collected_column_names.each do |name|
	          record_to_report["when"] = collected_columns[name][row_i]["T"]
	          record_to_report[name]   = collected_columns[name][row_i]["V"]
	        end
	        records_to_report << record_to_report
	      end
	      row_i = row_i + 1
	    end

	    @aggregate_columns = @columns[0 .. @columns.length]
	    @aggregate_columns.delete_if {|column| column.include? "Code" } 
	    @aggregate_columns.delete_if {|column| column.include? "_MC" } 
	    @records = Array.new
	    @minimums = Hash.new
	    @maximums = Hash.new
	    @averages = Hash.new
	    for aggregate_column in @aggregate_columns
	        @averages[aggregate_column] = 0
	    end

	    first_key = records_to_report.first.keys.first
	    for record_to_report in records_to_report do
	        record = Hash.new
	        begin
	          record["DateTime"] = Time.at(record_to_report[first_key]/1000).to_datetime
	        rescue
	        end

	        for column in @columns
	          record[column] = record_to_report[column]
	        end
	        
			if record["CEMS10_Stack_NOx_EmRate_HDM_Hourly"]
			    excess = record["CEMS10_Stack_NOx_EmRate_HDM_Hourly"] - LIMIT_WITHOUT_BURNER_HOURLY
			    if excess < 0 
			      excess = 0
			    end
			    record["outlet_nox_excess_without"] = excess
			    excess = record["CEMS10_Stack_NOx_EmRate_HDM_Hourly"] - LIMIT_WITH_BURNER_HOURLY
			    if excess < 0 
			      excess = 0
			    end
			    record["outlet_nox_excess_with"] = excess
			end
			if record["CEMS10_Stack_NOx_EmRate_HDM_Daily"]
			    excess = record["CEMS10_Stack_NOx_EmRate_HDM_Daily"] - LIMIT_WITHOUT_BURNER_DAILY
			    if excess < 0 
			      excess = 0
			    end
			    record["outlet_nox_excess_without"] = excess
			    excess = record["CEMS10_Stack_NOx_EmRate_HDM_Daily"] - LIMIT_WITH_BURNER_DAILY
			    if excess < 0 
			      excess = 0
			    end
			    record["outlet_nox_excess_with"] = excess
			end
			if record["CEMS10_Stack_NOx_EmRate_HDM_Monthly"]
			    excess = record["CEMS10_Stack_NOx_EmRate_HDM_Monthly"] - LIMIT_WITHOUT_BURNER_MONTHLY
			    if excess < 0 
			      excess = 0
			    end
			    record["outlet_nox_excess_without"] = excess
			    excess = record["CEMS10_Stack_NOx_EmRate_HDM_Monthly"] - LIMIT_WITH_BURNER_MONTHLY
			    if excess < 0 
			      excess = 0
			    end
			    record["outlet_nox_excess_with"] = excess
			end

	        @records << record
	        for aggregate_column in @aggregate_columns 
	            update_aggregates_for aggregate_column, record
	        end
	    end

	    # @records.map{ |record|
	    #     case record["CEMS10_MonitoringCode"]
	    #         when 0
	    #             record["CEMS10_MonitoringCode"] = "Normal Operation"
	    #         when 10
	    #             record["CEMS10_MonitoringCode"] = "Required Adjustment Not Made"
	    #         when 11
	    #             record["CEMS10_MonitoringCode"] = "Excess Drift Primary Analyzer"
	    #         when 12
	    #             record["CEMS10_MonitoringCode"] = "Excess Drift Ancillary Analyzer"
	    #         when 13
	    #             record["CEMS10_MonitoringCode"] = "Process Down"
	    #         when 14
	    #             record["CEMS10_MonitoringCode"] = "Recalibration"
	    #         when 15
	    #             record["CEMS10_MonitoringCode"] = "Preventive Maintenance"
	    #         when 16
	    #             record["CEMS10_MonitoringCode"] = "Primary Analyzer Malfunction"
	    #         when 17
	    #             record["CEMS10_MonitoringCode"] = "Ancillary Analyzer Malfunction"
	    #         when 18
	    #             record["CEMS10_MonitoringCode"] = "Data Handling System Malfunction"
	    #         when 19
	    #             record["CEMS10_MonitoringCode"] = "Sample Interface Malfunction"
	    #         when 20
	    #             record["CEMS10_MonitoringCode"] = "Corrective Maintenance"
	    #         when 21
	    #             record["CEMS10_MonitoringCode"] = "Blowback"
	    #         when 22
	    #             record["CEMS10_MonitoringCode"] = "Analyzer Over/Under Range"
	    #         when 23
	    #             record["CEMS10_MonitoringCode"] = "Unit Not Being Sampled"
	    #         when 98
	    #             record["CEMS10_MonitoringCode"] = "Auto-Calibration"
	    #         when 99
	    #             record["CEMS10_MonitoringCode"] = "Software Maintenance"
	    #     end
	    #     case record["CEMS10_ProcessCode"]
	    #         when 8
	    #             record["CEMS10_ProcessCode"] = "Normal Operation"
	    #         when 1
	    #             record["CEMS10_ProcessCode"] = "Monitor Equipment Malfucntion"
	    #         when 2
	    #             record["CEMS10_ProcessCode"] = "Control Equipment Malfunction (Non-Monitor Equipmemt Malfunction)"
	    #         when 3
	    #             record["CEMS10_ProcessCode"] = "Starting Up (Quality Assurance Calibrations)"
	    #         when 4
	    #             record["CEMS10_ProcessCode"] = "Shutting Down (Other Known Causes)"
	    #         when 5
	    #             record["CEMS10_ProcessCode"] = "Unit is Offline"
	    #         when 6
	    #             record["CEMS10_ProcessCode"] = "Clean Process Equipment"
	    #         when 7
	    #             record["CEMS10_ProcessCode"] = "Cleaning Equipment"
	    #     end
	    # }
	    for aggregate_column in @aggregate_columns
	      @averages[aggregate_column] = @averages[aggregate_column] / records_to_report.length
	    end
	    headers = ["Date"]
	    headers = headers + @columns_human.map {|row| row[1]}
	    CSV.open(Rails.root.join('public',"report.csv").to_s, "w") do |csv|
	        csv << headers
	        @records.each do |record|
	          csv << record.values
	        end
	    end
	rescue 
	    # render json: { error: "Prism returns no data" }, status: :not_found
	    # redirect_to root_url
	    # render file: "#{Rails.root}/public/prism_no_data.html"
	    render template: "errors/prism_no_data"
	end	

  private

	  def normalize_time_range_needed
	  	@from_human = request.GET["from"] + " " + request.GET["fromt"]
	    @to_human   = request.GET["to"] + " " + request.GET["tot"]
	    @interval   = request.GET["interval"].to_i * 1000 # milliseconds

	    fy = @from_human.split(" ")[2].to_i
	    fm = Date::ABBR_MONTHNAMES.index(@from_human.split(" ")[0]).to_i
	    fd = @from_human.split(" ")[1].to_i
	    fhr = @from_human.split(" ")[3].split(":")[0].to_i
	    fmn = @from_human.split(" ")[3].split(":")[1].to_i
	    fmeridian = @from_human.split(" ")[4]
	    if fhr == 12 and fmeridian == "AM"
        	logger.debug "normalize_time_range_needed: 12 AM " +  [fy,fm,fd,fhr,fmn].to_s
	    	fhr = 0
	    elsif fhr == 12 and fmeridian == "PM"
        	logger.debug "normalize_time_range_needed: 12 PM " +  [fy,fm,fd,fhr,fmn].to_s
	    	fhr = 12
	    else
	    	if fmeridian == "PM"
	        	logger.debug "normalize_time_range_needed:PM: " +  [fy,fm,fd,fhr,fmn].to_s
		    	fhr = fhr + 12
	    	end
	    end

	    ty = @to_human.split(" ")[2].to_i
	    tm = Date::ABBR_MONTHNAMES.index(@to_human.split(" ")[0]).to_i
	    td = @to_human.split(" ")[1].to_i
	    thr = @to_human.split(" ")[3].split(":")[0].to_i
	    tmn = @to_human.split(" ")[3].split(":")[1].to_i
	    tmeridian = @to_human.split(" ")[4]
	    if thr == 12 and tmeridian == "AM"
        	logger.debug "normalize_time_range_needed: 12 AM " +  [fy,fm,fd,fhr,fmn].to_s
	    	thr = 0
	    elsif thr == 12 and tmeridian == "PM"
        	logger.debug "normalize_time_range_needed: 12 PM " +  [fy,fm,fd,fhr,fmn].to_s
	    	thr = 12
	    else
	    	if tmeridian == "PM"
	        	logger.debug "normalize_time_range_needed:PM: " +  [fy,fm,fd,fhr,fmn].to_s
		    	thr = thr + 12
	    	end
	    end

	    @from_object = Time.new(fy,fm,fd,fhr,fmn)
        logger.debug "normalize_time_range_needed:fy,fm,fd,fhr,fmn: " +  [fy,fm,fd,fhr,fmn].to_s
	    @to_object   = Time.new(ty,tm,td,thr,tmn)
        logger.debug "normalize_time_range_needed:ty,tm,td,thr,tmn: " +  [ty,tm,td,thr,tmn].to_s

	    @from_unix = @from_object.to_i*1000
	    @to_unix   = @to_object.to_i*1000

	    @from_human  = Time.at(@from_unix/1000).ctime
	    @to_human    = Time.at(@to_unix/1000).ctime

        logger.debug "normalize_time_range_needed:objects: " +  @from_object.to_s + " " + @to_object.to_s
        logger.debug "normalize_time_range_needed:unix: "    +  @from_unix.to_s + " " + @to_unix.to_s
        logger.debug "normalize_time_range_needed:human: "   +  @from_human.to_s  + " " + @to_human.to_s
	  end
  
    def eliminate_bad_tags
      # puts "eliminating bad tags from: " + @tags_to_request.to_s
      @tags_to_request.each do |tag|
        @tags_to_request = @tags_to_request - [tag] unless tag_exist? tag 
      end
    end

    def tag_exist? tag
      # puts "tag_exist?"
      url = "https://10.101.206.21/webapi/api/trend/multitag?tags=" + tag + "&start=" + @from_unix.to_s + "&end=" + @to_unix.to_s + "&interval=" + @interval.to_s
      puts "  checking " + url
      begin
        result = HTTParty.get(url, :verify => false)
        if result["status"] == -1
          puts "  BAD TAG: " + tag
          puts "  because " + result.to_s
          return false
        else
          # puts "  good tag"
          return true
        end
      rescue Exception => e
        puts "  Exception: " + e.to_s
        return false
      end
      return true
    end

    def get_data
      puts "getting data"
      @@progress = 0
      @from_unix_ptr = @from_unix
      days_needed = @to_unix - @from_unix_ptr
      logger.debug "get_data:range: " + @from_unix.to_s + ", " + @to_unix.to_s
      # puts "  range pre: " + Time.at(@from_unix/1000).to_s + ", " + Time.at(@to_unix/1000).to_s
      while @from_unix_ptr < @to_unix do
			togo = days_needed - (@to_unix - @from_unix_ptr)
        	logger.debug "get_data:while: " + ((@to_unix - @from_unix)/1000/60/60/24).round(0).to_s
			@@progress = togo / days_needed
        	logger.debug "get_data:get_data:progress: " + @progress.to_s
			url = "https://10.101.206.21/webapi/api/trend/multitag?tags=" + @tags_to_request + "&start=" + @from_unix_ptr.to_s + "&end=" + @to_unix.to_s + "&interval=" + @interval.to_s
        	logger.debug url
			result = HTTParty.get(url, :verify => false)
			# puts "  result: " + result.class.to_s
			if result["message"] != "SUCCESS" then
              	logger.debug "get_data:Oh oh!"
				break
			else
				result_parsed = JSON.parse(result.to_s)["data"]
				@responses << result_parsed
			end
			first_key = result_parsed.keys.first
			@from_unix_ptr = result_parsed[first_key][result_parsed[first_key].length-1]["T"]
      end
      logger.debug "get_data:range: " + @from_unix.to_s + ", " + @to_unix.to_s
      logger.debug "get_data:@responses.length: " + @responses.length.to_s
    end

    def update_aggregates_for column, record
      @minimums[column] = record[column] if @minimums[column].nil?
      @maximums[column] = record[column] if @maximums[column].nil?
      @minimums[column] = record[column] < @minimums[column] ? record[column] : @minimums[column] unless record[column].nil? or @minimums[column].nil?
      @maximums[column] = record[column] > @maximums[column] ? record[column] : @maximums[column] unless record[column].nil? or @maximums[column].nil?
      @averages[column] = @averages[column] + record[column] unless record[column].nil? or @averages[column].nil?
    end

end
