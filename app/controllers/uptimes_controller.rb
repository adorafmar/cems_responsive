class UptimesController < ApplicationController

  def index

  end

  def show
    if valid? then
      operation_time = []
      @columns_human = [
        ["Hours","CEMS10_ProcessCode",nil],
      ]
      get_from_prism
      # @records.map! do |hash| 
      # 	{"DateTime" => hash["DateTime"],"CEMS10_ProcessCode" => "Normal Operation"}
      # end
      @records = collect_by_days @records
      @records.each do |when0, what|
      	string = when0.to_s
        year = string[0..4]
        month = string[4...6]
        day = string[6...8]
      	when_object = Time.new(year,month,day)
        ot, reasons = get_ot_reasons(what)
      	operation_time << Hash[{"DateTime" => when_object, "CEMS10_ProcessCode" => ot, "reasons" => reasons}]
      end
      @records = operation_time
    else
      redirect_to action: "index"
    end
  end

  def truncate_to_day time
  	return 10000*time.year + 100*time.month + time.day
  end

  def collect_by_days records
  	bins = {}
  	records.map do |record|
  		ndx = truncate_to_day record["DateTime"]
  		bin = bins[ndx]
  		if bin.nil?
  			bins[ndx] = [record]
  		else
  			bins[ndx] << record
  		end
  	end
  	return bins
  end

  def get_ot_reasons records
  	total = 0
    reasons = Set.new
    n_records = records.length
  	ndx = 0
  	t0 = records[ndx]["DateTime"].to_i
    y0 = records[ndx]["CEMS10_ProcessCode"].to_i
  	ndx = 1
  	while ndx != n_records
      t1 = records[ndx]["DateTime"].to_i
      y1 = records[ndx]["CEMS10_ProcessCode"].to_i
  		dt = (t1-t0)/3600
	  	if y0 == 8 or y0 == 3 or y0 == 6
        total = total + dt 
        reasons << y0
	  	end
  		t0 = t1
      y0 = y1
  		ndx = ndx + 1
  	end
  	return total, reasons
  end
end