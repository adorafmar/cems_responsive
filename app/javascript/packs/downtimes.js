console.log("Hello from downtimes.js!");
setInterval(() => {
	console.log("from callback")
	const Http = new XMLHttpRequest();
	const url='downtimes/progress.json';
	Http.open("GET", url);
	Http.send();
	Http.onreadystatechange=(e)=>{
		console.log("onreadystatechange");
		console.log("responseText: " + Http.responseText);
		let json = "";
		try {
			json = JSON.parse(Http.responseText);
		} catch (err) {
			console.log("ouch!");
		}
		const element = document.getElementById("progress");
		element.style.width = "" + json.progress + "%";
	}
}, 5000);