json.extract! quarterhourly, :id, :created_at, :updated_at
json.url quarterhourly_url(quarterhourly, format: :json)
