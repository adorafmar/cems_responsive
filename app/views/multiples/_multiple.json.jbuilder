json.extract! multiple, :id, :created_at, :updated_at
json.url multiple_url(multiple, format: :json)
