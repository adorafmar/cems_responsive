json.extract! monthly, :id, :created_at, :updated_at
json.url monthly_url(monthly, format: :json)
