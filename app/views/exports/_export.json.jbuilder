json.extract! export, :id, :created_at, :updated_at
json.url export_url(export, format: :json)
