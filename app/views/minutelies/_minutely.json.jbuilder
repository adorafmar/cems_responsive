json.extract! minutely, :id, :from, :to, :created_at, :updated_at
json.url minutely_url(minutely, format: :json)
