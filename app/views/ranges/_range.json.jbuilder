json.extract! range, :id, :from, :to, :created_at, :updated_at
json.url range_url(range, format: :json)
