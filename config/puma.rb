# workers Integer(ENV['WEB_CONCURRENCY'] || 2)
threads_count = Integer(ENV['THREAD_COUNT'] || 5)
threads threads_count, threads_count

rackup      DefaultRackup
port        3000
environment ENV['RACK_ENV'] || 'production'

# if ENV['RACK_ENV'] == 'development'
  localhost_key = "#{File.join('config', 'local-certs', 'localhost-key.pem')}"
  localhost_crt = "#{File.join('config', 'local-certs', 'localhost.pem')}"
  # To be able to use rake etc
  ssl_bind '0.0.0.0', 3001, {
    key: localhost_key,
    cert: localhost_crt,
    verify_mode: 'none'
  }
# end