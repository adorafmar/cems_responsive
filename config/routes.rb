Rails.application.routes.draw do
  get '/downtimes/progress', to: 'downtimes#progress'
  resources :anyparms
  resources :calevents
  resources :calibrates
  resources :dailies
  resources :downtimes
  resources :emissiondays
  resources :emissionmonths
  resources :emissions
  resources :exports
  resources :hourlies do
    collection do
      get 'progress'
    end
  end
  resources :menus
  resources :minutelies
  resources :monthlies
  resources :multiples
  resources :progresses
  resources :quarterhourlies
  resources :ranges
  resources :t1s
  resources :uptimes
  root "menus#index"
  # match "/404", to: "errors#not_found", via: :all
  # match "/500", to: "errors#internal_server_error", via: :all
end
