require "application_system_test_case"

class EmissionmonthsTest < ApplicationSystemTestCase
  setup do
    @emissionmonth = emissionmonths(:one)
  end

  test "visiting the index" do
    visit emissionmonths_url
    assert_selector "h1", text: "Emissionmonths"
  end

  test "creating a Emissionmonth" do
    visit emissionmonths_url
    click_on "New Emissionmonth"

    click_on "Create Emissionmonth"

    assert_text "Emissionmonth was successfully created"
    click_on "Back"
  end

  test "updating a Emissionmonth" do
    visit emissionmonths_url
    click_on "Edit", match: :first

    click_on "Update Emissionmonth"

    assert_text "Emissionmonth was successfully updated"
    click_on "Back"
  end

  test "destroying a Emissionmonth" do
    visit emissionmonths_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Emissionmonth was successfully destroyed"
  end
end
