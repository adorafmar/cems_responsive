require "application_system_test_case"

class CalibratesTest < ApplicationSystemTestCase
  setup do
    @calibrate = calibrates(:one)
  end

  test "visiting the index" do
    visit calibrates_url
    assert_selector "h1", text: "Calibrates"
  end

  test "creating a Calibrate" do
    visit calibrates_url
    click_on "New Calibrate"

    click_on "Create Calibrate"

    assert_text "Calibrate was successfully created"
    click_on "Back"
  end

  test "updating a Calibrate" do
    visit calibrates_url
    click_on "Edit", match: :first

    click_on "Update Calibrate"

    assert_text "Calibrate was successfully updated"
    click_on "Back"
  end

  test "destroying a Calibrate" do
    visit calibrates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Calibrate was successfully destroyed"
  end
end
