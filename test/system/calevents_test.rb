require "application_system_test_case"

class CaleventsTest < ApplicationSystemTestCase
  setup do
    @calevent = calevents(:one)
  end

  test "visiting the index" do
    visit calevents_url
    assert_selector "h1", text: "Calevents"
  end

  test "creating a Calevent" do
    visit calevents_url
    click_on "New Calevent"

    click_on "Create Calevent"

    assert_text "Calevent was successfully created"
    click_on "Back"
  end

  test "updating a Calevent" do
    visit calevents_url
    click_on "Edit", match: :first

    click_on "Update Calevent"

    assert_text "Calevent was successfully updated"
    click_on "Back"
  end

  test "destroying a Calevent" do
    visit calevents_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Calevent was successfully destroyed"
  end
end
