require "application_system_test_case"

class RollingsTest < ApplicationSystemTestCase
  setup do
    @rolling = rollings(:one)
  end

  test "visiting the index" do
    visit rollings_url
    assert_selector "h1", text: "Rollings"
  end

  test "creating a Rolling" do
    visit rollings_url
    click_on "New Rolling"

    click_on "Create Rolling"

    assert_text "Rolling was successfully created"
    click_on "Back"
  end

  test "updating a Rolling" do
    visit rollings_url
    click_on "Edit", match: :first

    click_on "Update Rolling"

    assert_text "Rolling was successfully updated"
    click_on "Back"
  end

  test "destroying a Rolling" do
    visit rollings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rolling was successfully destroyed"
  end
end
