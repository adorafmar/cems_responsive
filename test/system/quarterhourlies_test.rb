require "application_system_test_case"

class QuarterhourliesTest < ApplicationSystemTestCase
  setup do
    @quarterhourly = quarterhourlies(:one)
  end

  test "visiting the index" do
    visit quarterhourlies_url
    assert_selector "h1", text: "Quarterhourlies"
  end

  test "creating a Quarterhourly" do
    visit quarterhourlies_url
    click_on "New Quarterhourly"

    click_on "Create Quarterhourly"

    assert_text "Quarterhourly was successfully created"
    click_on "Back"
  end

  test "updating a Quarterhourly" do
    visit quarterhourlies_url
    click_on "Edit", match: :first

    click_on "Update Quarterhourly"

    assert_text "Quarterhourly was successfully updated"
    click_on "Back"
  end

  test "destroying a Quarterhourly" do
    visit quarterhourlies_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Quarterhourly was successfully destroyed"
  end
end
