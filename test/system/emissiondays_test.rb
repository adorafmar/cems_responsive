require "application_system_test_case"

class EmissiondaysTest < ApplicationSystemTestCase
  setup do
    @emissionday = emissiondays(:one)
  end

  test "visiting the index" do
    visit emissiondays_url
    assert_selector "h1", text: "Emissiondays"
  end

  test "creating a Emissionday" do
    visit emissiondays_url
    click_on "New Emissionday"

    click_on "Create Emissionday"

    assert_text "Emissionday was successfully created"
    click_on "Back"
  end

  test "updating a Emissionday" do
    visit emissiondays_url
    click_on "Edit", match: :first

    click_on "Update Emissionday"

    assert_text "Emissionday was successfully updated"
    click_on "Back"
  end

  test "destroying a Emissionday" do
    visit emissiondays_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Emissionday was successfully destroyed"
  end
end
