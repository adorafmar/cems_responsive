require "application_system_test_case"

class HourliesTest < ApplicationSystemTestCase
  setup do
    @hourly = hourlies(:one)
  end

  test "visiting the index" do
    visit hourlies_url
    assert_selector "h1", text: "Hourlies"
  end

  test "creating a Hourly" do
    visit hourlies_url
    click_on "New Hourly"

    click_on "Create Hourly"

    assert_text "Hourly was successfully created"
    click_on "Back"
  end

  test "updating a Hourly" do
    visit hourlies_url
    click_on "Edit", match: :first

    click_on "Update Hourly"

    assert_text "Hourly was successfully updated"
    click_on "Back"
  end

  test "destroying a Hourly" do
    visit hourlies_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Hourly was successfully destroyed"
  end
end
