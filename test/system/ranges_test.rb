require "application_system_test_case"

class RangesTest < ApplicationSystemTestCase
  setup do
    @range = ranges(:one)
  end

  test "visiting the index" do
    visit ranges_url
    assert_selector "h1", text: "Ranges"
  end

  test "creating a Range" do
    visit ranges_url
    click_on "New Range"

    fill_in "From", with: @range.from
    fill_in "To", with: @range.to
    click_on "Create Range"

    assert_text "Range was successfully created"
    click_on "Back"
  end

  test "updating a Range" do
    visit ranges_url
    click_on "Edit", match: :first

    fill_in "From", with: @range.from
    fill_in "To", with: @range.to
    click_on "Update Range"

    assert_text "Range was successfully updated"
    click_on "Back"
  end

  test "destroying a Range" do
    visit ranges_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Range was successfully destroyed"
  end
end
