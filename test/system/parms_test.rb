require "application_system_test_case"

class ParmsTest < ApplicationSystemTestCase
  setup do
    @parm = parms(:one)
  end

  test "visiting the index" do
    visit parms_url
    assert_selector "h1", text: "Parms"
  end

  test "creating a Parm" do
    visit parms_url
    click_on "New Parm"

    click_on "Create Parm"

    assert_text "Parm was successfully created"
    click_on "Back"
  end

  test "updating a Parm" do
    visit parms_url
    click_on "Edit", match: :first

    click_on "Update Parm"

    assert_text "Parm was successfully updated"
    click_on "Back"
  end

  test "destroying a Parm" do
    visit parms_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Parm was successfully destroyed"
  end
end
