require "application_system_test_case"

class DowntimesTest < ApplicationSystemTestCase
  setup do
    @downtime = downtimes(:one)
  end

  test "visiting the index" do
    visit downtimes_url
    assert_selector "h1", text: "Downtimes"
  end

  test "creating a Downtime" do
    visit downtimes_url
    click_on "New Downtime"

    click_on "Create Downtime"

    assert_text "Downtime was successfully created"
    click_on "Back"
  end

  test "updating a Downtime" do
    visit downtimes_url
    click_on "Edit", match: :first

    click_on "Update Downtime"

    assert_text "Downtime was successfully updated"
    click_on "Back"
  end

  test "destroying a Downtime" do
    visit downtimes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Downtime was successfully destroyed"
  end
end
