require "application_system_test_case"

class MultiplesTest < ApplicationSystemTestCase
  setup do
    @multiple = multiples(:one)
  end

  test "visiting the index" do
    visit multiples_url
    assert_selector "h1", text: "Multiples"
  end

  test "creating a Multiple" do
    visit multiples_url
    click_on "New Multiple"

    click_on "Create Multiple"

    assert_text "Multiple was successfully created"
    click_on "Back"
  end

  test "updating a Multiple" do
    visit multiples_url
    click_on "Edit", match: :first

    click_on "Update Multiple"

    assert_text "Multiple was successfully updated"
    click_on "Back"
  end

  test "destroying a Multiple" do
    visit multiples_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Multiple was successfully destroyed"
  end
end
