require "application_system_test_case"

class MinuteliesTest < ApplicationSystemTestCase
  setup do
    @minutely = minutelies(:one)
  end

  test "visiting the index" do
    visit minutelies_url
    assert_selector "h1", text: "Minutelies"
  end

  test "creating a Minutely" do
    visit minutelies_url
    click_on "New Minutely"

    fill_in "From", with: @minutely.from
    fill_in "To", with: @minutely.to
    click_on "Create Minutely"

    assert_text "Minutely was successfully created"
    click_on "Back"
  end

  test "updating a Minutely" do
    visit minutelies_url
    click_on "Edit", match: :first

    fill_in "From", with: @minutely.from
    fill_in "To", with: @minutely.to
    click_on "Update Minutely"

    assert_text "Minutely was successfully updated"
    click_on "Back"
  end

  test "destroying a Minutely" do
    visit minutelies_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Minutely was successfully destroyed"
  end
end
