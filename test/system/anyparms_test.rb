require "application_system_test_case"

class AnyparmsTest < ApplicationSystemTestCase
  setup do
    @anyparm = anyparms(:one)
  end

  test "visiting the index" do
    visit anyparms_url
    assert_selector "h1", text: "Anyparms"
  end

  test "creating a Anyparm" do
    visit anyparms_url
    click_on "New Anyparm"

    click_on "Create Anyparm"

    assert_text "Anyparm was successfully created"
    click_on "Back"
  end

  test "updating a Anyparm" do
    visit anyparms_url
    click_on "Edit", match: :first

    click_on "Update Anyparm"

    assert_text "Anyparm was successfully updated"
    click_on "Back"
  end

  test "destroying a Anyparm" do
    visit anyparms_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Anyparm was successfully destroyed"
  end
end
