require "test_helper"

class CaleventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @calevent = calevents(:one)
  end

  test "should get index" do
    get calevents_url
    assert_response :success
  end

  test "should get new" do
    get new_calevent_url
    assert_response :success
  end

  test "should create calevent" do
    assert_difference('Calevent.count') do
      post calevents_url, params: { calevent: {  } }
    end

    assert_redirected_to calevent_url(Calevent.last)
  end

  test "should show calevent" do
    get calevent_url(@calevent)
    assert_response :success
  end

  test "should get edit" do
    get edit_calevent_url(@calevent)
    assert_response :success
  end

  test "should update calevent" do
    patch calevent_url(@calevent), params: { calevent: {  } }
    assert_redirected_to calevent_url(@calevent)
  end

  test "should destroy calevent" do
    assert_difference('Calevent.count', -1) do
      delete calevent_url(@calevent)
    end

    assert_redirected_to calevents_url
  end
end
