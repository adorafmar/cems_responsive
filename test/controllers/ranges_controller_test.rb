require "test_helper"

class RangesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @range = ranges(:one)
  end

  test "should get index" do
    get ranges_url
    assert_response :success
  end

  test "should get new" do
    get new_range_url
    assert_response :success
  end

  test "should create range" do
    assert_difference('Range.count') do
      post ranges_url, params: { range: { from: @range.from, to: @range.to } }
    end

    assert_redirected_to range_url(Range.last)
  end

  test "should show range" do
    get range_url(@range)
    assert_response :success
  end

  test "should get edit" do
    get edit_range_url(@range)
    assert_response :success
  end

  test "should update range" do
    patch range_url(@range), params: { range: { from: @range.from, to: @range.to } }
    assert_redirected_to range_url(@range)
  end

  test "should destroy range" do
    assert_difference('Range.count', -1) do
      delete range_url(@range)
    end

    assert_redirected_to ranges_url
  end
end
