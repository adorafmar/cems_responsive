require "test_helper"

class AnyparmsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @anyparm = anyparms(:one)
  end

  test "should get index" do
    get anyparms_url
    assert_response :success
  end

  test "should get new" do
    get new_anyparm_url
    assert_response :success
  end

  test "should create anyparm" do
    assert_difference('Anyparm.count') do
      post anyparms_url, params: { anyparm: {  } }
    end

    assert_redirected_to anyparm_url(Anyparm.last)
  end

  test "should show anyparm" do
    get anyparm_url(@anyparm)
    assert_response :success
  end

  test "should get edit" do
    get edit_anyparm_url(@anyparm)
    assert_response :success
  end

  test "should update anyparm" do
    patch anyparm_url(@anyparm), params: { anyparm: {  } }
    assert_redirected_to anyparm_url(@anyparm)
  end

  test "should destroy anyparm" do
    assert_difference('Anyparm.count', -1) do
      delete anyparm_url(@anyparm)
    end

    assert_redirected_to anyparms_url
  end
end
