require "test_helper"

class CalibratesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @calibrate = calibrates(:one)
  end

  test "should get index" do
    get calibrates_url
    assert_response :success
  end

  test "should get new" do
    get new_calibrate_url
    assert_response :success
  end

  test "should create calibrate" do
    assert_difference('Calibrate.count') do
      post calibrates_url, params: { calibrate: {  } }
    end

    assert_redirected_to calibrate_url(Calibrate.last)
  end

  test "should show calibrate" do
    get calibrate_url(@calibrate)
    assert_response :success
  end

  test "should get edit" do
    get edit_calibrate_url(@calibrate)
    assert_response :success
  end

  test "should update calibrate" do
    patch calibrate_url(@calibrate), params: { calibrate: {  } }
    assert_redirected_to calibrate_url(@calibrate)
  end

  test "should destroy calibrate" do
    assert_difference('Calibrate.count', -1) do
      delete calibrate_url(@calibrate)
    end

    assert_redirected_to calibrates_url
  end
end
