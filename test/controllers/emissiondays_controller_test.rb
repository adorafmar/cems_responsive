require "test_helper"

class EmissiondaysControllerTest < ActionDispatch::IntegrationTest
  setup do
    @emissionday = emissiondays(:one)
  end

  test "should get index" do
    get emissiondays_url
    assert_response :success
  end

  test "should get new" do
    get new_emissionday_url
    assert_response :success
  end

  test "should create emissionday" do
    assert_difference('Emissionday.count') do
      post emissiondays_url, params: { emissionday: {  } }
    end

    assert_redirected_to emissionday_url(Emissionday.last)
  end

  test "should show emissionday" do
    get emissionday_url(@emissionday)
    assert_response :success
  end

  test "should get edit" do
    get edit_emissionday_url(@emissionday)
    assert_response :success
  end

  test "should update emissionday" do
    patch emissionday_url(@emissionday), params: { emissionday: {  } }
    assert_redirected_to emissionday_url(@emissionday)
  end

  test "should destroy emissionday" do
    assert_difference('Emissionday.count', -1) do
      delete emissionday_url(@emissionday)
    end

    assert_redirected_to emissiondays_url
  end
end
