require "test_helper"

class EmissionmonthsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @emissionmonth = emissionmonths(:one)
  end

  test "should get index" do
    get emissionmonths_url
    assert_response :success
  end

  test "should get new" do
    get new_emissionmonth_url
    assert_response :success
  end

  test "should create emissionmonth" do
    assert_difference('Emissionmonth.count') do
      post emissionmonths_url, params: { emissionmonth: {  } }
    end

    assert_redirected_to emissionmonth_url(Emissionmonth.last)
  end

  test "should show emissionmonth" do
    get emissionmonth_url(@emissionmonth)
    assert_response :success
  end

  test "should get edit" do
    get edit_emissionmonth_url(@emissionmonth)
    assert_response :success
  end

  test "should update emissionmonth" do
    patch emissionmonth_url(@emissionmonth), params: { emissionmonth: {  } }
    assert_redirected_to emissionmonth_url(@emissionmonth)
  end

  test "should destroy emissionmonth" do
    assert_difference('Emissionmonth.count', -1) do
      delete emissionmonth_url(@emissionmonth)
    end

    assert_redirected_to emissionmonths_url
  end
end
