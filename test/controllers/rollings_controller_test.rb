require "test_helper"

class RollingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rolling = rollings(:one)
  end

  test "should get index" do
    get rollings_url
    assert_response :success
  end

  test "should get new" do
    get new_rolling_url
    assert_response :success
  end

  test "should create rolling" do
    assert_difference('Rolling.count') do
      post rollings_url, params: { rolling: {  } }
    end

    assert_redirected_to rolling_url(Rolling.last)
  end

  test "should show rolling" do
    get rolling_url(@rolling)
    assert_response :success
  end

  test "should get edit" do
    get edit_rolling_url(@rolling)
    assert_response :success
  end

  test "should update rolling" do
    patch rolling_url(@rolling), params: { rolling: {  } }
    assert_redirected_to rolling_url(@rolling)
  end

  test "should destroy rolling" do
    assert_difference('Rolling.count', -1) do
      delete rolling_url(@rolling)
    end

    assert_redirected_to rollings_url
  end
end
