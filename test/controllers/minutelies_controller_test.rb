require "test_helper"

class MinuteliesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @minutely = minutelies(:one)
  end

  test "should get index" do
    get minutelies_url
    assert_response :success
  end

  test "should get new" do
    get new_minutely_url
    assert_response :success
  end

  test "should create minutely" do
    assert_difference('Minutely.count') do
      post minutelies_url, params: { minutely: { from: @minutely.from, to: @minutely.to } }
    end

    assert_redirected_to minutely_url(Minutely.last)
  end

  test "should show minutely" do
    get minutely_url(@minutely)
    assert_response :success
  end

  test "should get edit" do
    get edit_minutely_url(@minutely)
    assert_response :success
  end

  test "should update minutely" do
    patch minutely_url(@minutely), params: { minutely: { from: @minutely.from, to: @minutely.to } }
    assert_redirected_to minutely_url(@minutely)
  end

  test "should destroy minutely" do
    assert_difference('Minutely.count', -1) do
      delete minutely_url(@minutely)
    end

    assert_redirected_to minutelies_url
  end
end
