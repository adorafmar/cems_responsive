require "test_helper"

class HourliesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hourly = hourlies(:one)
  end

  test "should get index" do
    get hourlies_url
    assert_response :success
  end

  test "should get new" do
    get new_hourly_url
    assert_response :success
  end

  test "should create hourly" do
    assert_difference('Hourly.count') do
      post hourlies_url, params: { hourly: {  } }
    end

    assert_redirected_to hourly_url(Hourly.last)
  end

  test "should show hourly" do
    get hourly_url(@hourly)
    assert_response :success
  end

  test "should get edit" do
    get edit_hourly_url(@hourly)
    assert_response :success
  end

  test "should update hourly" do
    patch hourly_url(@hourly), params: { hourly: {  } }
    assert_redirected_to hourly_url(@hourly)
  end

  test "should destroy hourly" do
    assert_difference('Hourly.count', -1) do
      delete hourly_url(@hourly)
    end

    assert_redirected_to hourlies_url
  end
end
