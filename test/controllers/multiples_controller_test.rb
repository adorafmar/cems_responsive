require "test_helper"

class MultiplesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @multiple = multiples(:one)
  end

  test "should get index" do
    get multiples_url
    assert_response :success
  end

  test "should get new" do
    get new_multiple_url
    assert_response :success
  end

  test "should create multiple" do
    assert_difference('Multiple.count') do
      post multiples_url, params: { multiple: {  } }
    end

    assert_redirected_to multiple_url(Multiple.last)
  end

  test "should show multiple" do
    get multiple_url(@multiple)
    assert_response :success
  end

  test "should get edit" do
    get edit_multiple_url(@multiple)
    assert_response :success
  end

  test "should update multiple" do
    patch multiple_url(@multiple), params: { multiple: {  } }
    assert_redirected_to multiple_url(@multiple)
  end

  test "should destroy multiple" do
    assert_difference('Multiple.count', -1) do
      delete multiple_url(@multiple)
    end

    assert_redirected_to multiples_url
  end
end
