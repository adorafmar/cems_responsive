require "test_helper"

class ParmsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @parm = parms(:one)
  end

  test "should get index" do
    get parms_url
    assert_response :success
  end

  test "should get new" do
    get new_parm_url
    assert_response :success
  end

  test "should create parm" do
    assert_difference('Parm.count') do
      post parms_url, params: { parm: {  } }
    end

    assert_redirected_to parm_url(Parm.last)
  end

  test "should show parm" do
    get parm_url(@parm)
    assert_response :success
  end

  test "should get edit" do
    get edit_parm_url(@parm)
    assert_response :success
  end

  test "should update parm" do
    patch parm_url(@parm), params: { parm: {  } }
    assert_redirected_to parm_url(@parm)
  end

  test "should destroy parm" do
    assert_difference('Parm.count', -1) do
      delete parm_url(@parm)
    end

    assert_redirected_to parms_url
  end
end
