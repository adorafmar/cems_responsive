require "test_helper"

class QuarterhourliesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @quarterhourly = quarterhourlies(:one)
  end

  test "should get index" do
    get quarterhourlies_url
    assert_response :success
  end

  test "should get new" do
    get new_quarterhourly_url
    assert_response :success
  end

  test "should create quarterhourly" do
    assert_difference('Quarterhourly.count') do
      post quarterhourlies_url, params: { quarterhourly: {  } }
    end

    assert_redirected_to quarterhourly_url(Quarterhourly.last)
  end

  test "should show quarterhourly" do
    get quarterhourly_url(@quarterhourly)
    assert_response :success
  end

  test "should get edit" do
    get edit_quarterhourly_url(@quarterhourly)
    assert_response :success
  end

  test "should update quarterhourly" do
    patch quarterhourly_url(@quarterhourly), params: { quarterhourly: {  } }
    assert_redirected_to quarterhourly_url(@quarterhourly)
  end

  test "should destroy quarterhourly" do
    assert_difference('Quarterhourly.count', -1) do
      delete quarterhourly_url(@quarterhourly)
    end

    assert_redirected_to quarterhourlies_url
  end
end
